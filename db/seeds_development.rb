seeds = YAML.load_file File.join(__dir__, 'seeds.yml')

# Put your development seeds here
FactoryBot.create :user_known
FactoryBot.create :user_admin_known

# Note: on models with 'has_many :xxx, class_name: "Yyy"', there is an issue
# with Spring causing the classes to be somehow reloaded. To avoid
# "ActiveRecord::AssociationTypeMismatch", errors during seeding, we always
# recreate the map instance whenever we need one.
FactoryBot.create :map, :user_submitted, name: 'My first map', center: [397, 561.5], user: User.first

seeds[:genera].each do |genus|
  Genus.create! name:        genus[:name],
                description: genus[:description],
                source:      genus[:source],
                kingdom:     genus[:kingdom]
end

seeds[:layers].each do |layer|
  Layer.create! layer
end

seeds[:resources].each do |resource|
  Resource.create! name:         resource[:name],
                   description:  resource[:description],
                   source:       resource[:source],
                   edible:       resource[:edible],
                   common_names: resource[:common_names],
                   layer:        Layer.find_by(name: resource[:layer]),
                   genus:        Genus.find_by(name: resource[:genus])
end

seeds[:resources].each do |resource| # rubocop:disable Style/CombinableLoops
  next if resource[:parent].blank?

  Resource.find_by(name: resource[:name]).update parent: Resource.find_by(name: resource[:parent])
end
