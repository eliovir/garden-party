class CreateGenera < ActiveRecord::Migration[6.0]
  def change
    create_table :genera do |t|
      t.string :name, null: false, default: nil
      t.text :description, null: false, default: nil
      t.integer :kingdom, null: false, default: nil

      t.timestamps
    end

    add_reference :resources, :genus, null: false, foreign_key: true # rubocop:disable Rails/NotNullColumn
  end
end
