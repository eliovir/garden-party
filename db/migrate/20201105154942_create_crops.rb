class CreateCrops < ActiveRecord::Migration[6.0]
  def change
    create_table :crops do |t|
      t.string   :name
      t.datetime :planted_at

      t.references :resource, null: false, foreign_key: true
      t.references :patch, null: false, foreign_key: true

      t.timestamps
    end
  end
end
