class CreateLayers < ActiveRecord::Migration[6.0]
  def change
    create_table :layers do |t|
      t.string :name,      null: false, default: nil
      t.text :description, null: false, default: nil
      t.string :color,     null: false, default: nil

      t.timestamps
    end
  end
end
