class AddDateFieldsToActivities < ActiveRecord::Migration[6.0]
  def change
    change_table :activities, bulk: true do |t|
      t.datetime :done_at, after: :notes
      t.datetime :planned_for, after: :notes
    end

    reversible do |dir|
      dir.up do
        Activity.all.each { |a| a.update done_at: a.created_at }
      end
    end
  end
end
