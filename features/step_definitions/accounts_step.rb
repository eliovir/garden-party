Given("I have an account and i'm logged in") do
  @signed_in_user = FactoryBot.create :user
  visit '/'
  click_on I18n.t('layouts.application.sign_in')

  within '.devise-form' do
    fill_in I18n.t('activerecord.attributes.user.email'), with: @signed_in_user.email
    fill_in I18n.t('activerecord.attributes.user.password'), with: 'password'

    click_on I18n.t('devise.sessions.new.sign_in')
  end
end
