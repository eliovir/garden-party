Feature: Inventory
  As an user
  In order to have a plain list of the elements in my garden
  I want to have a kind of inventory page

  Background:
    Given I have an account and i'm logged in
    And I have a map named "My garden"
    And there is a resource named "Pipe-weed"

  Scenario: Access the inventory page
    Given I access the "My garden" map
    When I click on the inventory link
    Then I see the inventory page

  Scenario: Display of point patches
    Given I have a tree named "Boris the tree"
    And I access the inventory page for "My garden"
    Then I see "Boris the tree" as a point patch

  Scenario: Display of shaped patches
    Given I have a patch named "Field of North Lindon"
    And I access the inventory page for "My garden"
    Then I see "Field of North Lindon" as a shaped patch

  Scenario: Check unplaced elements
    Given I have a tree named "The tall One"
    And I have a patch named "Aromatics" with some "Pepper" in it
    When I access the inventory page for "My garden"
    Then I see that "The tall One" is unplanted in the point patches list
    And I see that "Pepper" is unplanted in the "Aromatics" elements list

  Scenario: Add elements to a patch
    Given I have a patch named "Field of North Lindon"
    And I access the inventory page for "My garden"
    When I add some "Pipe-weed" in the "Field of North Lindon"
    And I see that "Pipe-weed" is unplanted in the "Field of North Lindon" elements list

  Scenario: Place an element now
    Given I have a tree named "The tall One"
    And I access the inventory page for "My garden"
    When I place "The tall One" now
    Then I see that "The tall One" has available actions

  Scenario: Place an element later
    Given I have a tree named "The tall One"
    And I access the inventory page for "My garden"
    When I plan to place "The tall One" tomorrow
    Then I see that "The tall One" should be planted tomorrow

  Scenario: Place a planned element
    Given I have a tree named "The tall One" which I should implant tomorrow
    And I access the inventory page for "My garden"
    When I validate the implantation of "The tall One"
    Then I see that "The tall One" has available actions

  Scenario: Remove a planted element now
    Given I have a planted tree named "Apple tree"
    And I access the inventory page for "My garden"
    When I remove the "Apple tree" now
    Then I don't see the "Apple tree" anymore

  Scenario: Remove a planned element
    Given I have a tree named "The tall One" which I should remove tomorrow
    And I access the inventory page for "My garden"
    When I validate the removal of "The tall One"
    Then I don't see the "The tall One" anymore

  Scenario: Plan a watering later
    Given I have a planted tree named "The weird bush"
    And I planned to water "The weird bush" tomorrow
    And I access the inventory page for "My garden"
    When I check the watering state for "The weird bush"
    Then I see that "The weird bush" should be watered tomorrow

  Scenario: See last watering actions
    Given I have a planted tree named "Cat tree"
    And I watered the "Cat tree" every days for 3 days
    And I access the inventory page for "My garden"
    When I check the watering state for "Cat tree"
    Then I see I watered the "Cat tree" the last 3 days

  Scenario: See overdue watering
    Given I have a planted tree named "Shrub"
    And I planned to water "Shrub" yesterday
    And I access the inventory page for "My garden"
    When I check the watering state for "Shrub"
    When I see that "Shrub" watering is overdue

  Scenario: Finish planned watering
    Given I have a planted tree named "Elmr"
    And I planned to water "Elmr" today
    And I access the inventory page for "My garden"
    And I check the watering state for "Elmr"
    When I finish watering "Elmr"
    Then I see that "Elmr" was watered today

  Scenario: Display actions log
    Given I have a planted tree named "The tree"
    And I watered the "The tree" every days for 3 days
    And I fertilized the "The tree" every days for 3 days
    And I access the inventory page for "My garden"
    When I check the action log for "The tree"
    Then I see 6 actions
