require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module GardenParty
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 6.0

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration can go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded after loading
    # the framework and any gems in your application.
    config.garden_party = config_for(:garden_party)
    config.action_mailer.default_url_options = config.garden_party.default_url_options
    config.action_mailer.default_options = config.garden_party.default_email_options

    # Generate JS translations
    config.middleware.use I18n::JS::Middleware
  end
end
