Rails.application.routes.draw do
  devise_for :users

  root to: 'app#home'

  get '/app', to: 'app#app'
  get '/changelog', to: 'app#changelog'

  namespace :api do
    resources :activities, except: [:index]
    resources :elements, except: [:index] do
      resources :activities, only: [:index]
    end
    resources :resource_interactions
    resources :layers
    resources :maps do
      get :picture, on: :member
      resources :activities, only: [:index]
      resources :patches, only: [:index]
      resources :elements, only: [:index]
    end
    resources :patches, except: [:index] do
      resources :elements, only: [:index]
    end
    resources :genera
    resources :resources do
      resources :resource_interactions, only: [:index], path: 'interactions'
    end
  end

  namespace :admin do
    resources :layers
    resources :genera
    resources :resource_interactions
    resources :resources
  end
end
