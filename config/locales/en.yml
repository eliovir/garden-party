---
en:
  activerecord:
    attributes:
      activity:
        created_at: Created at
        done_at: Done at
        name: Name
        notes: Notes
        planned_for: Planned for
        subject: Subject
        subject_type: Type
        updated_at: Updated at
      element:
        created_at: Created at
        implantation_planned_for: Implantation planned for
        implanted_at: Implanted at
        patch: Patch
        removal_planned_for: Removal planned for
        removed_at: Removed at
        resource: Resource
        updated_at: Updated at
      genus:
        created_at: Created at
        description: Description
        kingdom: Kingdom
        name: Name
        source: Source
        updated_at: Updated at
      layer:
        color: Color
        created_at: Created at
        description: Description
        name: Name
        updated_at: Updated at
      map:
        center: Center
        created_at: Created at
        name: Name
        updated_at: Updated at
        user: Owner
      patch:
        created_at: Created at
        geometry: Geometry
        map: Map
        name: Name
        updated_at: Updated at
      resource:
        common_names: Common names
        created_at: Created at
        description: Description
        edible: Edible
        genus: Genus
        layer: Layer
        name: Name
        parent: Parent
        source: Source
        updated_at: Updated at
      resource_interaction:
        created_at: Created at
        nature: Nature
        notes: Notes
        subject: Subject
        target: Target
        updated_at: Updated at
      user:
        confirmation_sent_at: Confirmation sent at
        confirmation_token: Confirmation token
        confirmed_at: Confirmed at
        created_at: Created at
        email: Email
        encrypted_password: Encrypted password
        remember_created_at: Remember created at
        reset_password_sent_at: Reset password sent at
        reset_password_token: Reset password token
        role: Role
        unconfirmed_email: Unconfirmed email
        updated_at: Updated at
        username: Username
    errors:
      bad_image_format: is not an allowed format (%{formats})
      bad_point_format: don't have the required format ([x, y])
      element:
        date_before_implantation: can't be before implantation date
      feature:
        bad_feature_format: 'has the following errors: %{errors}'
        open_polygon: is open
        radius_not_allowed: should not have a radius
      models:
        patch:
          elements:
            element_missing: has no resource
            too_much_elements: has too much resource
      not_array: is not a list
      not_strings_array: is not a list of strings
    models:
      activity:
        one: Activity
        other: Activities
      element:
        one: Element
        other: Elements
      genus:
        one: Genus
        other: Genera
      layer:
        one: Layer
        other: Layers
      map:
        one: Map
        other: Maps
      patch:
        one: Patch
        other: Patches
      resource:
        one: Resource
        other: Resources
      resource_interaction:
        one: Interaction
        other: Interactions
      user:
        one: User
        other: Users
  admin:
    genera:
      create:
        success: Genus was successfully created
      destroy:
        success: Genus was successfully destroyed
      edit:
        title: Editing a genus
      form:
        errors:
          one: 'An error prevented us to save this genus:'
          other: "%{count} errors prevented us to save this genus:"
      index:
        new: New genus
        no_content: No genus has been recorded so far.
        title: Genera
      new:
        title: New genus
      show:
        title: 'Genus: %{name}'
      update:
        success: Genus was successfully updated
    layers:
      create:
        success: Layer was successfully created
      destroy:
        success: Layer was successfully destroyed
      edit:
        title: Editing a layer
      form:
        errors:
          one: 'An error prevented us to save this layer:'
          other: "%{count} errors prevented us to save this layer:"
      index:
        new: New layer
        no_content: No layer has been recorded so far.
        title: Layers
      new:
        title: New layer
      show:
        title: 'Layer: %{name}'
      update:
        success: Layer was successfully updated
    resource_interactions:
      create:
        success: Interaction was successfully created
      destroy:
        success: Interaction was successfully destroyed
      edit:
        title: Editing interaction
      form:
        errors:
          one: 'An error prevented us to save this interaction:'
          other: "%{count} errors prevented us to save this interaction:"
      index:
        new: New interaction
        no_content: No interaction has been recorded so far.
        title: Interactions
      new:
        title: New interaction
      show:
        title: Interaction
      update:
        success: Interaction was successfully updated
    resources:
      create:
        success: Resource was successfully created
      destroy:
        success: Resource was successfully destroyed
      edit:
        title: Editing a resource
      form:
        errors:
          one: 'An error prevented us to save this resource:'
          other: "%{count} errors prevented us to save this resource:"
      index:
        new: New resource
        no_content: No resource has been recorded so far.
        title: Resources
      new:
        title: New resource
      show:
        no_child_resource: No resource has been created
        resources: Resources
        title: 'Resource: %{name}'
      update:
        success: Resource was successfully updated
  application_controller:
    error_not_authorized:
      not_authorized: You are not allowed to perform this action.
  controller:
    application:
      error_csrf:
        error: Invalid CSRF token
      error_not_found:
        error: The requested resource was not found.
      error_unprocessable:
        error: Provided data in unprocessable.
  generic:
    are_you_sure: Are you sure?
    back: Back
    cancel: Cancel
    comma_separated_values: List of comma separated values
    definition: "%{value}:"
    destroy: Destroy
    edit: Edit
    info: Info
    loading: Loading
    'no': 'No'
    ok: OK
    save: Save
    show: Show
    'yes': 'Yes'
  js:
    activities:
      form:
        date: Date
    activity:
      failed_to_create: Failed to create this element
      successfully_created: Action saved
    api:
      forbidden: Acces to this resource is forbidden
      internal_server_error: An internal server error happened
      page_not_found: 'Page not found: %{url}'
      unauthorized: You don't have enough rights to perform this action
      unknown_error: An error happened (%{code})
      unprocessable_entity: We couln't save the record because of a form error
      unreachable_server: Server is unreachable
    element:
      failed_to_destroy: Failed to destroy this element
      successfully_destroyed: Element successfully destroyed
    elements:
      action_buttons:
        action:
          fertilize: Fertilize
          observe: Observe
          remove: Remove
          trim: Trim
          water: Water
        destroy_element: Destroy element totally
        implant: Set it up
        plantation_date: Plantation date
        removal_date: Removal date
        removed_at: Removed at %{date}
        show_activity: Show activity
        waiting_for_implantation_on: Waiting for implantation on %{date}
      activities:
        action: Action
        fertilized: Fertilized
        no_activity: No activity recorded.
        noted: Noted
        notes: Notes
        trimmed: Trimmed
        watered: Watered
      item_content:
        failed_to_implant: Failed to implant element
        failed_to_remove: Failed to remove element
        successfully_implanted: Element successfully implanted
        successfully_removed: Element successfully removed
      last_action:
        destroy_help: Action will be removed from history.
        destroyed_successfully: Action destroyed
        finish: Finish
        finished_successfully: Action finished
    genera:
      index:
        genus: Genus
        layer: Layer
        name: Name
    generic:
      form:
        has_errors: 'Please check the form on the following points:'
      load_error: Errors occurred when loading application data. Please try again.
      messages:
        destroy_warning_history: Element will be deleted with its history.
        destroy_warning_history_multiple: Elements will be deleted with their history.
    layouts:
      garden:
        main_menu:
          current_map: 'Current map: %{name}'
          home: Home
          library: Library
          maps: Maps
        map_not_found: Sorry, map not found.
        popup:
          add_resource: Add resource
        side_panel:
          button_destroy_selected: Delete %{amount} selected patches
          button_draw_circle: Draw circle
          button_draw_point: Draw point
          button_draw_polygon: Draw polygon
          button_pointer_mode: Pointer mode
          button_select_mode: Select mode
          help:
            drawing_mode: Drawing mode
            drawing_mode_text_1: |
              In Drawing mode, you may place single elements (ideal for tree, bushes, animal spots, ...),
              or draw shapes representing a patch.
            drawing_mode_text_2: |
              In a patch, you may have many elements, but they are not visible as single points (ideal
              when you have many elements of the same kind in a small area).
            edition_mode: Edition mode
            edition_mode_text: In edition mode, you may move elements and resize patches, as well as managing the elements in a patch.
            review_mode: Review mode
            review_mode_text: |
              You are in "Review" mode: you can select elements or patches and perform actions on them,
              as watering, trimming, taking notes,...
          nothing_selected: Nothing selected
          placing_resource: Placing "%{name}"
          please_select_resource: Please select a resource to place
        toolbar:
          add_things: Add things
          resource_overview: Resources overview
          todo: Todo
    map:
      destroy_confirm: All map data will be permanently destroyed.
      failed_to_destroy: Failed to destroy the map
      index:
        no_map_available: You have no map yet. Why don't you create one ?
      successfully_destroyed: Map successfully destroyed
    maps:
      form:
        background_picture: Background picture
        map_type: Map type
        name: Name
        name_placeholder: My garden
        save_this_map: Save this map
        select_center_and: Select the point representing the center of your garden, and
        select_picture: Select a picture
        use_osm_background: Use OpenStreetMap background
        use_your_own_picture: Use your own picture
      index:
        new_map: New map
        title: Your maps
    models:
      patch:
        generic_name: A patch
    patch:
      failed_to_create: Failed to create the patch
      failed_to_destroy: Failed to destroy the patch
      failed_to_update: Failed to update the patch
      item:
        add_resource: Add resource
        type_of_patch: 'Type of patch: %{type}'
      no_element_in_this_patch: There is nothing (anymore) in this patch.
      successfully_created: Patch successfully created
      successfully_destroyed: Patch successfully destroyed
      successfully_updated: Patch successfully updated
    patches:
      form:
        name: Name
      index:
        alone_resources: Alone resources
        editing_patch: Editing patch
        no_patches: There are no patches in this map
        no_point: There are no alone resources in this map
        patches: Patches
        resource_selection: Element selection
    resources:
      description_area:
        common_names: Common names
        dislikes: Dislikes
        edible: Edible
        genus: Genus
        group: Group
        kingdom: Kingdom
        layer: Layer
        likes: Likes
        no_known_resource: No known resource.
        source: Source
      picker:
        enter_name: Resource name
        search: Search
    todos:
      activity:
        element_in_patch: "%{elementName} (in %{patchName})"
      index:
        no_activities: You have no planned activities.
    views:
      elements:
        item_content:
          implant_now: Implant now
          remove_now: Remove now
  js_dates:
    date:
      day: numeric
      month: short
      year: numeric
    day_name:
      weekday: short
    medium:
      day: numeric
      hour: numeric
      minute: numeric
      month: short
      year: numeric
  layouts:
    application:
      account: Account
      app: App
      changelog: Changelog
      genera: Genera
      home: Home
      layers: Layers
      resource_interactions: Interactions
      resources: Resources
      sign_in: Sign in
      sign_out: Sign out
