<%
  # Base template:
  # rspec-rails/lib/generators/rspec/scaffold/templates/request_spec.rb

  # Custom vars from available generator methods/instance vars:
  # i.e.: member
  namespace_name = class_path.length > 0 ? class_path.first : nil
  # i.e.: Member
  namespace = namespace_name ? namespace_name.classify : nil
  # i.e.: Post
  model_name = @file_name.classify
  # i.e.: post (without namespace)
  singular_table_name_only = file_name
  # i.e.: posts (without namespace)
  plural_table_name_only = controller_class_name.demodulize.underscore
  # i.e: Posts (without namespace)
  controller_class_name_only = controller_class_name.demodulize
  # i.e: member_post_path
  entity_redirect = namespace.blank? ? "#{model_name}.last" : "#{namespace_name}_#{singular_table_name_only}_path(#{model_name}.last)"
-%>
require 'rails_helper'

<% module_namespacing do -%>
RSpec.describe "/<%= name.underscore.pluralize %>", <%= type_metatag(:request) %> do

  let(:valid_attributes) {
    FactoryBot.build(:<%= singular_table_name_only %>).attributes
  }
  let(:invalid_attributes) {
    skip("Add a hash of attributes invalid for your model")
  }

<%- if namespace_name == 'admin' -%>
  include_context 'with authenticated admin'

<%- end -%>
<% unless options[:singleton] -%>
  describe "GET <%= namespace_name ? "/#{namespace_name}" : '' %>/<%= plural_table_name_only %>" do
    it "returns a success response" do
      <%= model_name %>.create! valid_attributes
      get <%= index_helper %>_url
      expect(response).to be_successful
    end
  end

<% end -%>
  describe "GET <%= namespace_name ? "/#{namespace_name}" : '' %>/<%= plural_table_name_only %>/1" do
    it "returns a success response" do
      <%= model_name %>.create! valid_attributes
      get <%= show_helper.sub(/@#{namespace_name}_?/, '') %>
      expect(response).to be_successful
    end
  end

  describe "GET /new" do
    it "returns a success response" do
      get <%= new_helper %>
      expect(response).to be_successful
    end
  end

  describe "GET <%= namespace_name ? "/#{namespace_name}" : '' %>/<%= plural_table_name_only %>/1/edit" do
    it "returns a success response" do
      <%= file_name %> = <%= model_name %>.create! valid_attributes
      get <%= edit_helper.sub(/@#{namespace_name}_?/,'') %>
      expect(response).to be_successful
    end
  end

  describe "POST <%= namespace_name ? "/#{namespace_name}" : '' %>/<%= plural_table_name_only %>" do
    context "with valid params" do
      it "creates a new <%= model_name %>" do
        expect {
          post <%= index_helper %>_url, params: { <%= file_name %>: valid_attributes }
        }.to change(<%= model_name %>, :count).by(1)
      end

      it "redirects to the created <%= model_name %>" do
        post <%= index_helper %>_url, params: {<%= file_name %>: valid_attributes}
        expect(response).to redirect_to(<%= entity_redirect %>)
      end
    end

    context "with invalid params" do
      it "returns a success response (i.e. to display the 'new' template)" do
        post <%= index_helper %>_url, params: {<%= file_name %>: invalid_attributes}
        expect(response).to be_successful
      end
    end
  end

  describe "PUT <%= namespace_name ? "/#{namespace_name}" : '' %>/<%= plural_table_name_only %>/1" do
    let(:<%= file_name %>) { <%= model_name %>.create! valid_attributes }

    context "with valid params" do
      let(:new_attributes) {
        skip("Add a hash of attributes valid for your model")
      }

      it "updates the requested <%= ns_file_name %>" do
        put <%= show_helper.sub(/@#{namespace_name}_?/, '') %>, params: {<%= file_name %>: new_attributes}
        <%= file_name %>.reload
        skip("Add assertions for updated state")
      end

      it "redirects to the <%= ns_file_name %>" do
        put <%= show_helper.sub(/@#{namespace_name}_?/, '') %>, params: {<%= file_name %>: new_attributes}
        expect(response).to redirect_to(<%= entity_redirect %>)
      end
    end

    context "with invalid params" do
      it "returns a success response (i.e. to display the 'edit' template)" do
        put <%= show_helper.sub(/@#{namespace_name}_?/, '') %>, params: {<%= file_name %>: invalid_attributes}
        expect(response).to be_successful
      end
    end
  end

  describe "DELETE <%= namespace_name ? "/#{namespace_name}" : '' %>/<%= plural_table_name_only %>/1" do
    it "destroys the requested <%= ns_file_name %>" do
      <%= file_name %> = <%= model_name %>.create! valid_attributes
      expect {
        delete <%= show_helper.sub(/@#{namespace_name}_?/, '') %>
      }.to change(<%= model_name %>, :count).by(-1)
    end

    it "redirects to the <%= table_name %> list" do
      <%= file_name %> = <%= model_name %>.create! valid_attributes
      delete <%= show_helper.sub(/@#{namespace_name}_?/, '') %>
      expect(response).to redirect_to(<%= index_helper %>_url)
    end
  end
end
<% end -%>
