<%
  # Base template:
# rspec-rails/lib/generators/rspec/scaffold/templates/api_request_spec.rb

# Custom vars from available generator methods/instance vars:
# i.e.: member
namespace_name = class_path.length > 0 ? class_path.first : nil
# i.e.: Member
namespace = namespace_name ? namespace_name.classify : nil
# i.e.: Post
model_name = @file_name.classify
# i.e.: post (without namespace)
singular_table_name_only = file_name
# i.e.: posts (without namespace)
plural_table_name_only = controller_class_name.demodulize.underscore
# i.e: Posts (without namespace)
controller_class_name_only = controller_class_name.demodulize
# i.e: member_post_path
entity_redirect = namespace.blank? ? "#{model_name}.last" : "#{namespace_name}_#{singular_table_name_only}_path(#{model_name}.last)"
-%>
require 'rails_helper'

<% module_namespacing do -%>
RSpec.describe "/<%= name.underscore.pluralize %>", <%= type_metatag(:request) %> do
  let(:valid_attributes) {
    FactoryBot.build(:<%= singular_table_name_only %>).attributes
  }

  let(:invalid_attributes) {
    skip("Add a hash of attributes invalid for your model")
  }

  let(:valid_headers) {
    {}
  }

<% unless options[:singleton] -%>
  describe "GET <%= namespace_name ? "/#{namespace_name}" : '' %>/index" do
    it "renders a successful response" do
      <%= model_name %>.create! valid_attributes
      get <%= index_helper %>_url, headers: valid_headers, as: :json
      expect(response).to be_successful
    end
  end
<% end -%>

  describe "GET <%= namespace_name ? "/#{namespace_name}" : '' %>/<%= plural_table_name_only %>/1" do
    it "renders a successful response" do
      <%= file_name %> = <%= model_name %>.create! valid_attributes
      get <%= show_helper.sub(/@#{namespace_name}_?/, '') %>, as: :json
      expect(response).to be_successful
    end
  end

  describe "POST <%= namespace_name ? "/#{namespace_name}" : '' %>/create" do
    context "with valid parameters" do
      it "creates a new <%= model_name %>" do
        expect do
          post <%= index_helper %>_url, params: { <%= file_name %>: valid_attributes }, headers: valid_headers, as: :json
        end.to change(<%= model_name %>, :count).by(1)
      end

      it "renders an successful response" do
        post <%= index_helper %>_url, params: { <%= file_name %>: valid_attributes }, headers: valid_headers, as: :json
        expect(response).to have_http_status(:created)
      end

      it "renders the new <%= model_name %> as JSON" do
        post <%= index_helper %>_url, params: { <%= file_name %>: valid_attributes }, headers: valid_headers, as: :json
        expect(response.content_type).to match(a_string_including("application/json"))
      end
    end

    context "with invalid parameters" do
      it "does not create a new <%= model_name %>" do
        expect do
          post <%= index_helper %>_url, params: { <%= file_name %>: invalid_attributes }, as: :json
        end.to change(<%= model_name %>, :count).by(0)
      end

      it "renders an error response" do
        post <%= index_helper %>_url, params: { <%= file_name %>: invalid_attributes }, headers: valid_headers, as: :json
        expect(response).to have_http_status(:unprocessable_entity)
      end

      it "renders the error as JSON" do
        post <%= index_helper %>_url, params: { <%= file_name %>: invalid_attributes }, headers: valid_headers, as: :json
        expect(response.content_type).to match(a_string_including("application/json"))
      end
    end
  end

  describe "PUT <%= namespace_name ? "/#{namespace_name}" : '' %>/<%= plural_table_name_only %>/1" do
    context "with valid parameters" do
      let(:new_attributes) do
        skip("Add a hash of attributes valid for your model")
      end

      it "updates the requested <%= model_name %>" do
        <%= file_name %> = <%= model_name %>.create! valid_attributes
        patch <%= show_helper.sub(/@#{namespace_name}_?/, '') %>, params: { <%= file_name %>: new_attributes }, headers: valid_headers, as: :json
        <%= file_name %>.reload
        skip("Add assertions for updated state")
      end

      it "renders a succes response" do
        <%= file_name %> = <%= model_name %>.create! valid_attributes
        patch <%= show_helper.sub(/@#{namespace_name}_?/, '') %>, params: { <%= file_name %>: new_attributes }, headers: valid_headers, as: :json
        expect(response).to have_http_status(:ok)
      end

      it "renders a JSON response with the <%= file_name %>" do
        <%= file_name %> = <%= model_name %>.create! valid_attributes
        patch <%= show_helper.sub(/@#{namespace_name}_?/, '') %>, params: { <%= file_name %>: new_attributes }, headers: valid_headers, as: :json
        expect(response.content_type).to match(a_string_including("application/json"))
      end
    end

    context "with invalid parameters" do
      it "renders an error response" do
        <%= file_name %> = <%= model_name %>.create! valid_attributes
        patch <%= show_helper.sub(/@#{namespace_name}_?/, '') %>, params: { <%= file_name %>: invalid_attributes }, headers: valid_headers, as: :json
        expect(response).to have_http_status(:unprocessable_entity)
      end

      it "renders the error as JSON" do
        <%= file_name %> = <%= model_name %>.create! valid_attributes
        patch <%= show_helper.sub(/@#{namespace_name}_?/, '') %>, params: { <%= file_name %>: invalid_attributes }, headers: valid_headers, as: :json
        expect(response.content_type).to match(a_string_including("application/json"))
      end
    end
  end

  describe "DELETE <%= namespace_name ? "/#{namespace_name}" : '' %>/<%= plural_table_name_only %>/1" do
    it "destroys the requested <%= file_name %>" do
      <%= file_name %> = <%= model_name %>.create! valid_attributes
      expect do
        delete <%= show_helper.sub(/@#{namespace_name}_?/, '') %>, headers: valid_headers, as: :json
      end.to change(<%= model_name %>, :count).by(-1)
    end
  end
end
<% end -%>
