class FrontGenerator
  EXCLUDED_MODELS = [
    'ApplicationRecord',
  ].freeze

  EXCLUDED_FIELDS = {
    'User' => %w[password_digest auth_tokens confirmation_token confirmation_sent_at reset_password_token reset_password_sent_at invitation_token invitation_sent_at invitation_accepted_at],
  }.freeze

  TYPES = {
    types:   {
      'EnumType'          => 'String',
      'Integer'           => 'number',
      'Jsonb'             => 'Object',
      'Point'             => '{x:number, y:number}',
      'String'            => 'String',
      'Text'              => 'String',
      'TimeZoneConverter' => 'Date|String',
    },
    setters: {
      'Point'             => ->(field) { "[#{field}.x, #{field}.y]" },
      'TimeZoneConverter' => ->(field) { "#{field} ? new Date(#{field}) : null" },
    },
  }.freeze

  def generate
    load_models
    load_attributes
    render 'module', File.join('stores', 'modules'), 'Module'
    render 'model', File.join('classes', 'models')
  end

  private

  def load_models
    return @models if @models

    @models = {}
    Dir.glob(Rails.root.join('app', 'models', '*.rb')).each do |filename|
      class_name = File.basename(filename, '.rb').classify
      next if EXCLUDED_MODELS.include? class_name

      @models[class_name] = {}
    end
  end

  def load_attributes
    raise 'Models not loaded' unless @models

    @models.each_key do |class_name|
      class_name.constantize.attribute_types.each do |attribute, type|
        next if EXCLUDED_FIELDS[class_name]&.include? attribute

        @models[class_name][attribute] = { type: type.class.name.demodulize, limit: type.limit }
      end
    end
  end

  def render(template_type, js_output_directory, suffix = '')
    template = File.read(File.join(__dir__, 'front_generator', 'templates', "#{template_type}.js.erb"))
    renderer = ERB.new(template)
    # "attributes" is used in templates
    @models.each do |model_name, attributes|
      File.write Rails.root.join('app', 'javascript', 'vue', js_output_directory, "#{model_name}#{suffix}.js"), renderer.result(binding)
    end
  end

  def attribute_type(attribute, details)
    type = TYPES.dig :types, details[:type]
    type ||= details[:type]
    type = "null|#{type}" if attribute == 'id'

    type
  end

  def attribute_setter(attribute, details)
    setter = TYPES.dig :setters, details[:type]
    return setter.call(attribute) if setter

    attribute
  end
end
