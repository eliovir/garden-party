class ResourcePictureGenerator
  OUTPUT_BASE = Rails.root.join('public', 'pictures').freeze

  def initialize(resource)
    @resource         = resource
    @layer            = resource.layer
    @base_file_name   = resource.picture_name
    @output_directory = Rails.root.join(OUTPUT_BASE, 'resources', resource.layer_id.to_s)
    @main_picture = File.join(@output_directory, "#{@base_file_name}.svg")
    @pattern_picture = File.join(@output_directory, "#{@base_file_name}_pattern.svg")
    FileUtils.mkdir_p @output_directory unless File.directory? @output_directory
  end

  def generate
    create_main_picture
    create_pattern_picture
  end

  def destroy_pictures
    FileUtils.rm @main_picture if File.exist? @main_picture
    FileUtils.rm @pattern_picture if File.exist? @pattern_picture
  end

  private

  def create_main_picture
    border_color = "rgb(#{@layer.color})"
    fill_color   = "rgba(#{@layer.color},0.2)"
    data = [
      '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 6.35 6.35">',
      "<path d=\"M6.35 3.175A3.175 3.175 0 013.175 6.35 3.175 3.175 0 010 3.175 3.175 3.175 0 013.175 0 3.175 3.175 0 016.35 3.175z\" fill=\"#{fill_color}\" stroke=\"#{border_color}\" stroke-width=\"0.1\"/>",
      '<text style="line-height:1.25" x="1.161" y="5.007" fill="rgba(0,0,0,0.5)" stroke="rgba(0,0,0,0.5)" font-weight="400" font-size="5.325" font-family="sans-serif" stroke-width="0.1">',
      "<tspan x=\"1.161\" y=\"5.000\">#{@resource.name.first}</tspan>",
      '</text></svg>',
    ].join
    File.write @main_picture, data
  end

  def create_pattern_picture
    # border_color = "rgb(#{@layer.color})"
    fill_color = "rgba(#{@layer.color},0.2)"
    data = [
      '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 6.35 6.35">',
      "<path d=\"M0 0h6.35v6.35H0z\" fill=\"#{fill_color}\"/>",
      '<text style="line-height:1.25" x="1.161" y="5.007" fill="rgba(0,0,0,0.5)" stroke="transparent" font-weight="400" font-size="2.5" font-family="sans-serif" stroke-width="0.1">',
      "<tspan x=\"1.161\" y=\"5.000\">#{@resource.name.first}</tspan>",
      '</text></svg>',
    ].join
    File.write @pattern_picture, data
  end
end
