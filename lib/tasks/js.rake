require 'classes/front_generator'

namespace :js do
  desc 'Generate models and VueX modules in /tmp/js'
  task generate: :environment do
    generator = FrontGenerator.new
    generator.generate
  end
end
