SWAGGER_FILE = File.join('public', 'swagger.json')

namespace :swagger do
  def remove_responses(hash)
    hash.each_value do |path|
      path.each_value do |method|
        method.except! 'responses'
      end
    end
    hash
  end

  def remove_example_values(hash)
    hash.each_value do |path|
      path.each_value do |method|
        next unless method.key? 'requestBody'

        method['requestBody']['content'].each_value do |format|
          format['examples'].each_value do |code|
            code.except! 'value'
          end
        end
      end
    end
    hash
  end

  def clean_paths(hash)
    hash = remove_responses hash
    remove_example_values hash
  end

  def should_commit?(old_version, new_version)
    new_version['paths'] = clean_paths new_version['paths']
    old_version['paths'] = clean_paths old_version['paths']

    new_version != old_version
  end

  desc 'Check if definitions changed in swagger file'
  task changed: :environment do
    raw_old_version = `git show HEAD~1:#{SWAGGER_FILE}`
    old_version     = JSON.parse raw_old_version

    raw_new_version = File.read SWAGGER_FILE
    new_version     = JSON.parse raw_new_version

    if should_commit? old_version, new_version
      warn "Yes, you should commit #{SWAGGER_FILE}"
    else
      warn "Nope, you should checkout #{SWAGGER_FILE}"
    end
  end

  # FIXME: Propose patch to rspec_rails_api to have a pretty JSON output
  desc 'Normalizes swagger.json'
  task normalize: :environment do
    hash = JSON.parse File.read(SWAGGER_FILE)
    hash.deep_sort!

    File.write Rails.root.join(SWAGGER_FILE), JSON.pretty_generate(hash)
  end
end
