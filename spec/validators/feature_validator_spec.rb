require 'rails_helper'

# FIXME: schema validator errors are not localized
RSpec.describe FeatureValidator do
  context 'when feature is invalid geoJSON feature' do
    it 'does not validate' do
      entity = FactoryBot.build :patch, geometry: ''

      expect do
        entity.validate!
      end.to raise_error(/did not match the following type: object/)
    end
  end

  context 'when feature is a Point' do
    context "when feature don't have enough coordinates" do
      it 'does not validates' do
        entity = FactoryBot.build :patch, :point
        entity.geometry['geometry']['coordinates'] = []

        # FIXME:  Schema validator will fail on closest match, which brings weird errors
        expect do
          entity.validate!
        end.to raise_error(/did not match one of the following values: Polygon/)
      end
    end
  end

  context 'when feature is a circle' do
    context "when feature don't have enough coordinates" do
      it 'does not validates' do
        entity = FactoryBot.build :patch, :circle
        entity.geometry['geometry']['coordinates'] = []

        # FIXME:  Schema validator will fail on closest match, which brings weird errors
        expect do
          entity.validate!
        end.to raise_error(/did not match one of the following values: Polygon/)
      end
    end
  end

  context 'when feature is a Polygon' do
    context 'when feature has a "radius" property' do
      it 'does not validates' do
        entity = FactoryBot.build :patch, :polygon
        entity.geometry['properties'] = { 'radius' => nil }

        expect do
          entity.validate!
        end.to raise_error(/of type object did not match/)
      end
    end

    context "when feature don't have enough coordinates" do
      it 'does not validates' do
        entity = FactoryBot.build :patch, :polygon
        entity.geometry['geometry']['coordinates'] = [[[0, 0], [10, 10], [0, 0]]]

        expect do
          entity.validate!
        end.to raise_error(/did not contain a minimum number of items 4/)
      end
    end

    context 'when feature is not closed' do
      it 'does not validates' do
        entity = FactoryBot.build :patch, :polygon
        entity.geometry['geometry']['coordinates'] = [[[0, 0], [10, 10], [20, 20], [30, 30]]]

        expect do
          entity.validate!
        end.to raise_error(/n'est pas fermé/)
      end
    end
  end
end
