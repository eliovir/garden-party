require 'rails_helper'

RSpec.describe '/admin/layers', type: :request do
  let(:valid_attributes) do
    FactoryBot.build(:layer).attributes
  end
  let(:invalid_attributes) do
    { name: '' }
  end

  include_context 'with authenticated admin'

  describe 'GET /admin/layers' do
    it 'returns a success response' do
      Layer.create! valid_attributes
      get admin_layers_url
      expect(response).to be_successful
    end
  end

  describe 'GET /admin/layers/1' do
    it 'returns a success response' do
      layer = Layer.create! valid_attributes
      get admin_layer_url(layer)
      expect(response).to be_successful
    end
  end

  describe 'GET #new' do
    it 'returns a success response' do
      get new_admin_layer_url
      expect(response).to be_successful
    end
  end

  describe 'GET /admin/layers/1/edit' do
    it 'returns a success response' do
      layer = Layer.create! valid_attributes
      get edit_admin_layer_url(layer)
      expect(response).to be_successful
    end
  end

  describe 'POST /admin/layers' do
    context 'with valid params' do
      it 'creates a new Layer' do
        expect do
          post admin_layers_url, params: { layer: valid_attributes }
        end.to change(Layer, :count).by(1)
      end

      it 'redirects to the created admin_layer' do
        post admin_layers_url, params: { layer: valid_attributes }
        expect(response).to redirect_to(admin_layer_path(Layer.last))
      end
    end

    context 'with invalid params' do
      it "returns a success response (i.e. to display the 'new' template)" do
        post admin_layers_url, params: { layer: invalid_attributes }
        expect(response).to be_successful
      end
    end
  end

  describe 'PUT /admin/layers/1' do
    let(:layer) { Layer.create! valid_attributes }

    context 'with valid params' do
      let(:new_attributes) do
        { name: 'New name' }
      end

      it 'updates the requested admin_layer' do
        put admin_layer_url(layer), params: { layer: new_attributes }
        layer.reload
        expect(layer.name).to eq new_attributes[:name]
      end

      it 'redirects to the admin_layer' do
        put admin_layer_url(layer), params: { layer: new_attributes }
        expect(response).to redirect_to(admin_layer_path(Layer.last))
      end
    end

    context 'with invalid params' do
      it "returns a success response (i.e. to display the 'edit' template)" do
        put admin_layer_url(layer), params: { layer: invalid_attributes }
        expect(response).to be_successful
      end
    end
  end

  describe 'DELETE /admin/layers/1' do
    it 'destroys the requested admin_layer' do
      layer = Layer.create! valid_attributes
      expect do
        delete admin_layer_url(layer)
      end.to change(Layer, :count).by(-1)
    end

    it 'redirects to the admin_layers list' do
      layer = Layer.create! valid_attributes
      delete admin_layer_url(layer)
      expect(response).to redirect_to(admin_layers_url)
    end
  end
end
