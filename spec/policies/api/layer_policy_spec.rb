require 'rails_helper'
require 'pundit/rspec'

RSpec.describe Api::LayerPolicy, type: :policy do
  let(:user) { FactoryBot.create :user }
  let(:user_admin) { FactoryBot.create :user_admin }
  let(:layer) { FactoryBot.create :layer }

  permissions '.scope' do
    before { FactoryBot.create_list :layer, 2 }

    context 'with no authenticated user' do
      it 'returns no layers' do
        expect(Pundit.policy_scope!(nil, [:api, Layer]).all.count).to eq 0
      end
    end

    context 'with authenticated user' do
      it 'returns all the layers' do
        expect(Pundit.policy_scope!(user, [:api, Layer]).all.count).to eq 2
      end
    end
  end

  permissions :index? do
    context 'with no authenticated user' do
      it 'denies access' do
        expect(described_class).not_to permit(nil, Layer)
      end
    end

    context 'with authenticated user' do
      it 'grants access' do
        expect(described_class).to permit(user, Layer)
      end
    end
  end

  permissions :show? do
    context 'with no authenticated user' do
      it 'denies access' do
        expect(described_class).not_to permit(nil, layer)
      end
    end

    context 'with authenticated user' do
      it 'grants access' do
        expect(described_class).to permit(user, layer)
      end
    end
  end

  permissions :create? do
    context 'with no authenticated user' do
      it 'denies access' do
        expect(described_class).not_to permit(nil, Layer)
      end
    end

    context 'with authenticated user' do
      it 'denies access' do
        expect(described_class).not_to permit(user, Layer)
      end
    end

    context 'with authenticated admin' do
      it 'grants access' do
        expect(described_class).to permit(user_admin, Layer)
      end
    end
  end

  permissions :update?, :destroy? do
    context 'with no authenticated user' do
      it 'denies access' do
        expect(described_class).not_to permit(nil, layer)
      end
    end

    context 'with authenticated user' do
      it 'denies access' do
        expect(described_class).not_to permit(user, layer)
      end
    end

    context 'with authenticated admin' do
      it 'grants access' do
        expect(described_class).to permit(user_admin, layer)
      end
    end
  end
end
