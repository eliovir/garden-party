require 'rails_helper'

RSpec.describe AppController, type: :routing do
  describe 'routing' do
    it 'routes to #home' do
      expect(get: '/').to route_to('app#home')
    end

    it 'routes to #app' do
      expect(get: '/app').to route_to('app#app')
    end
  end
end
