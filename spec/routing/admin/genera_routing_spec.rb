require 'rails_helper'

RSpec.describe Admin::GeneraController, type: :routing do
  describe 'routing' do
    it 'routes to #index' do
      expect(get: '/admin/genera').to route_to('admin/genera#index')
    end

    it 'routes to #new' do
      expect(get: '/admin/genera/new').to route_to('admin/genera#new')
    end

    it 'routes to #show' do
      expect(get: '/admin/genera/1').to route_to('admin/genera#show', id: '1')
    end

    it 'routes to #edit' do
      expect(get: '/admin/genera/1/edit').to route_to('admin/genera#edit', id: '1')
    end

    it 'routes to #create' do
      expect(post: '/admin/genera').to route_to('admin/genera#create')
    end

    it 'routes to #update via PUT' do
      expect(put: '/admin/genera/1').to route_to('admin/genera#update', id: '1')
    end

    it 'routes to #update via PATCH' do
      expect(patch: '/admin/genera/1').to route_to('admin/genera#update', id: '1')
    end

    it 'routes to #destroy' do
      expect(delete: '/admin/genera/1').to route_to('admin/genera#destroy', id: '1')
    end
  end
end
