require 'rails_helper'

RSpec.describe Admin::LayersController, type: :routing do
  describe 'routing' do
    it 'routes to #index' do
      expect(get: '/admin/layers').to route_to('admin/layers#index')
    end

    it 'routes to #new' do
      expect(get: '/admin/layers/new').to route_to('admin/layers#new')
    end

    it 'routes to #show' do
      expect(get: '/admin/layers/1').to route_to('admin/layers#show', id: '1')
    end

    it 'routes to #edit' do
      expect(get: '/admin/layers/1/edit').to route_to('admin/layers#edit', id: '1')
    end

    it 'routes to #create' do
      expect(post: '/admin/layers').to route_to('admin/layers#create')
    end

    it 'routes to #update via PUT' do
      expect(put: '/admin/layers/1').to route_to('admin/layers#update', id: '1')
    end

    it 'routes to #update via PATCH' do
      expect(patch: '/admin/layers/1').to route_to('admin/layers#update', id: '1')
    end

    it 'routes to #destroy' do
      expect(delete: '/admin/layers/1').to route_to('admin/layers#destroy', id: '1')
    end
  end
end
