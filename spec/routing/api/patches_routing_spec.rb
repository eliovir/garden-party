require 'rails_helper'

RSpec.describe Api::PatchesController, type: :routing do
  describe 'routing' do
    it 'routes to #index' do
      expect(get: '/api/maps/1/patches').to route_to(controller: 'api/patches', action: 'index', map_id: '1')
    end

    it 'routes to #show' do
      expect(get: '/api/patches/1').to route_to('api/patches#show', id: '1')
    end

    it 'routes to #create' do
      expect(post: '/api/patches').to route_to('api/patches#create')
    end

    it 'routes to #update via PUT' do
      expect(put: '/api/patches/1').to route_to('api/patches#update', id: '1')
    end

    it 'routes to #update via PATCH' do
      expect(patch: '/api/patches/1').to route_to('api/patches#update', id: '1')
    end

    it 'routes to #destroy' do
      expect(delete: '/api/patches/1').to route_to('api/patches#destroy', id: '1')
    end
  end
end
