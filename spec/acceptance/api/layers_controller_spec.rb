require 'rails_helper'
require 'acceptance_helper'

RSpec.describe Api::LayersController, type: :acceptance do
  resource 'Layers', 'Manage layers'

  entity :layer,
         id:           { type: :integer, description: 'Entity identifier' },
         name:         { type: :string, description: 'Layer name' },
         description:  { type: :string, description: 'Layer description' },
         border_color: { type: :string, description: 'RGB color to use for shapes border' },
         fill_color:   { type: :string, description: 'RGBA color to use to fill shapes' },
         created_at:   { type: :datetime, description: 'Creation date' },
         updated_at:   { type: :datetime, description: 'Update date' }

  entity :error,
         error: { type: :string, description: 'The error' }

  parameters :create_update_payload,
             name:        { type: :string, description: 'Layer name' },
             description: { type: :string, description: 'Layer description' },
             color:       { type: :string, description: 'Base color to decline as border and fill colors. Must be something like "255,255,255"' }

  parameters :path_params,
             id: { type: :integer, description: 'Target entity identifier' }

  context 'with authenticated member' do
    include_context 'with authenticated member'

    on_get('/api/layers', 'List layers') do
      for_code 200 do |url|
        FactoryBot.create_list :layer, 2
        visit url

        expect(response).to have_many defined :layer
      end
    end

    on_get('/api/layers/:id', 'Show one layer') do
      path_params defined: :path_params

      for_code 200 do |url|
        layer = FactoryBot.create :layer
        visit url, path_params: { id: layer.id }

        expect(response).to have_one defined :layer
      end

      for_code 404 do |url|
        visit url, path_params: { id: 0 }

        expect(response).to have_one defined :error
      end
    end
  end

  context 'with authenticated admin' do
    include_context 'with authenticated admin'

    on_post('/api/layers', 'Create new layer') do
      request_params defined: :create_update_payload

      for_code 201 do |url|
        layer_attributes = FactoryBot.build(:layer).attributes
        visit url, payload: layer_attributes
      end

      for_code 422 do |url|
        visit url, payload: { name: '' }
      end
    end

    on_put('/api/layers/:id', 'Update a layer') do
      path_params defined: :path_params
      request_params defined: :create_update_payload
      let(:layer) { FactoryBot.create :layer }

      for_code 200 do |url|
        visit url, path_params: { id: layer.id }, payload: { name: 'New name' }
      end

      for_code 422 do |url|
        visit url, path_params: { id: layer.id }, payload: { name: '' }
      end
    end

    on_delete('/api/layers/:id', 'Destroys a layer') do
      path_params defined: :path_params
      let(:layer) { FactoryBot.create :layer }

      for_code 204 do |url|
        visit url, path_params: { id: layer.id }
      end
    end
  end
end
