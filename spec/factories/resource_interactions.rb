FactoryBot.define do
  factory :resource_interaction do
    association :subject, factory: [:resource]
    association :target, factory: [:resource]
    nature { 0 }
  end
end
