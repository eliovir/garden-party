FactoryBot.define do
  factory :map do
    name { Faker::Lorem.sentence(word_count: rand(2...5)).tr('.', '') }
    center { [rand(10_000), rand(10_000)] }
    user

    trait :user_submitted do
      center { [rand(1000), rand(700)] }
      picture { Rack::Test::UploadedFile.new Rails.root.join('spec', 'fixtures', 'files', 'map.svg'), 'image/svg+xml' }
    end

    trait :dangerous do
      center { [rand(1000), rand(700)] }
      picture { Rack::Test::UploadedFile.new Rails.root.join('spec', 'fixtures', 'files', 'dangerous_map.svg'), 'image/svg+xml' }
    end

    trait :jpg do
      center { [rand(1000), rand(700)] }
      picture { Rack::Test::UploadedFile.new Rails.root.join('spec', 'fixtures', 'files', 'map.jpg'), 'image/jgp' }
    end

    trait :unsupported do
      center { [rand(1000), rand(700)] }
      picture { Rack::Test::UploadedFile.new Rails.root.join('spec', 'fixtures', 'files', 'map.txt'), 'plain/text' }
    end
  end
end
