FactoryBot.define do
  factory :element do
    resource
    patch

    trait :implanted do
      implanted_at { Date.current }
    end
  end
end
