FactoryBot.define do
  factory :genus do
    name { Faker::Lorem.word.titleize }
    description { Faker::Markdown.sandwich }
    # Default trait
    plant

    trait :plant do
      kingdom { 'plant' }
    end

    trait :animal do
      kingdom { 'animal' }
    end
  end
end
