FactoryBot.define do
  factory :layer do
    name { Faker::Lorem.sentence(word_count: rand(1...3)).tr('.', '') }
    description { Faker::Markdown.sandwich }
    color { Faker::Color.rgb_color.join(',') }
  end
end
