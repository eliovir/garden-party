FactoryBot.define do
  factory :activity do
    name { Faker::Verb.ing_form }
    # Traits
    for_element
    done

    trait :done do
      done_at { Time.current }
    end

    trait :planned do
      planned_for { Time.current + 1.day }
    end

    trait :for_element do
      association :subject, factory: :element
    end

    trait :for_patch do
      association :subject, factory: :patch
    end
  end
end
