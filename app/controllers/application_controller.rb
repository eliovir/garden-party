class ApplicationController < ActionController::Base
  include Pundit
  before_action :configure_permitted_parameters, if: :devise_controller?

  rescue_from ActiveRecord::RecordNotFound, with: :error_not_found
  rescue_from ActionController::InvalidAuthenticityToken, with: :error_csrf
  rescue_from ActionController::ParameterMissing, with: :error_unprocessable
  rescue_from Pundit::NotAuthorizedError, with: :error_not_authorized

  private

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:username])
    devise_parameter_sanitizer.permit(:account_update, keys: [:username])
  end

  def error_fallback(exception, fallback_message, status)
    message = exception&.message || fallback_message
    respond_to do |format|
      format.json { render json: { error: message }, status: status }
      format.html { raise exception }
    end
  end

  def error_not_found(exception = nil)
    # FIXME: i18n string: 'Requested content was not found'
    error_fallback(exception, I18n.t('controller.application.error_not_found.error'), :not_found)
  end

  def error_unprocessable(exception = nil)
    # FIXME: i18n string: 'Unprocessable entity'
    error_fallback(exception, I18n.t('controller.application.error_unprocessable.error'), :unprocessable_entity)
  end

  def error_csrf(exception = nil)
    # FIXME: i18n string: 'Invalid CSRF token'
    error_fallback(exception, I18n.t('controller.application.error_csrf.error'), :unprocessable_entity)
  end

  def error_not_authorized(_exception = nil)
    # FIXME: i18n string: 'You are not authorized to perform this action'
    message = I18n.t('application_controller.error_not_authorized.not_authorized')
    respond_to do |format|
      format.json { render json: { error: message }, status: :unauthorized }
      format.html { redirect_to new_user_session_path }
    end
  end
end
