module Admin
  class AdminApplicationController < ApplicationController
    before_action :authenticate_admin!

    private

    def authenticate_admin!
      authenticate_user!

      redirect_to new_user_session_url, status: :unauthorized unless current_user.admin?
    end

    def policy_scope(scope)
      super([:admin, scope])
    end

    def authorize(record, query = nil)
      super([:admin, record], query)
    end
  end
end
