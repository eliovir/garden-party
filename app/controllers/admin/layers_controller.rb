module Admin
  class LayersController < AdminApplicationController
    before_action :set_layer, only: [:show, :edit, :update, :destroy]

    # GET /admin/layers
    def index
      @layers = policy_scope(Layer)
    end

    # GET /admin/layers/1
    def show; end

    # GET /admin/layers/new
    def new
      authorize Layer

      @layer = Layer.new
    end

    # GET /admin/layers/1/edit
    def edit; end

    # POST /admin/layers
    def create
      authorize Layer

      @layer = Layer.new(layer_params)

      if @layer.save
        redirect_to admin_layer_path(@layer), notice: I18n.t('admin.layers.create.success')
      else
        render :new
      end
    end

    # PATCH/PUT /admin/layers/1
    def update
      if @layer.update(layer_params)
        redirect_to admin_layer_path(@layer), notice: I18n.t('admin.layers.update.success')
      else
        render :edit
      end
    end

    # DELETE /admin/layers/1
    def destroy
      @layer.destroy
      redirect_to admin_layers_url, notice: I18n.t('admin.layers.destroy.success')
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_layer
      @layer = Layer.find(params[:id])

      authorize @layer
    end

    # Only allow a trusted parameter "white list" through.
    def layer_params
      params.require(:layer).permit(:name, :color, :description)
    end
  end
end
