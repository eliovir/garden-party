module Api
  # FIXME: Find a way to DRY with ApplicationController
  class ApiApplicationController < ActionController::API
    include Pundit

    rescue_from ActiveRecord::RecordNotFound, with: :error_not_found
    rescue_from ActionController::InvalidAuthenticityToken, with: :error_csrf
    rescue_from ActionController::ParameterMissing, with: :error_unprocessable
    rescue_from Pundit::NotAuthorizedError, with: :error_not_authorized

    private

    def error_fallback(exception, fallback_message, status)
      message = exception&.message || fallback_message
      render json: { error: message }, status: status
    end

    def error_not_found(exception = nil)
      error_fallback(exception, I18n.t('controller.application.error_not_found.error'), :not_found)
    end

    def error_unprocessable(exception = nil)
      error_fallback(exception, I18n.t('controller.application.error_unprocessable.error'), :unprocessable_entity)
    end

    def error_csrf(exception = nil)
      error_fallback(exception, I18n.t('controller.application.error_csrf.error'), :unprocessable_entity)
    end

    def error_not_authorized(_exception = nil)
      message = I18n.t('application_controller.error_not_authorized.not_authorized')
      render json: { error: message }, status: :unauthorized
    end

    def policy_scope(scope)
      super([:api, scope])
    end

    def authorize(record, query = nil)
      super([:api, record], query)
    end
  end
end
