module Api
  class ActivitiesController < ApiApplicationController
    before_action :set_activity, only: [:show, :update, :destroy]

    # GET /api/activities
    def index
      @activities = policy_scope(Activity)
      @activities = if params[:element_id]
                      @activities.for_element params[:element_id]
                    elsif params[:map_id]
                      @activities.for_map params[:map_id]
                    end

      @activities.where(done_at: nil) if params[:pending]

      render 'api/activities/index'
    end

    # GET /api/activities/1
    def show
      render 'api/activities/show'
    end

    # POST /api/activities
    def create
      authorize Activity
      @activity = Activity.new(create_activity_params)

      if @activity.save
        render 'api/activities/show', status: :created, location: api_activity_url(@activity)
      else
        render json: @activity.errors, status: :unprocessable_entity
      end
    end

    # PATCH/PUT /api/activities/1
    def update
      if @activity.update(update_activity_params)
        render 'api/activities/show', status: :ok, location: api_activity_url(@activity)
      else
        render json: @activity.errors, status: :unprocessable_entity
      end
    end

    # DELETE /api/activities/1
    def destroy
      @activity.destroy
    end

    private

    # Use callbacks to share common setup or constraints between activities.
    def set_activity
      @activity = Activity.find(params[:id])

      authorize @activity
    end

    # Only allow a trusted parameter "white list" through.
    def create_activity_params
      params.require(:activity).permit(:name, :notes, :planned_for, :done_at, :subject_id, :subject_type)
    end

    def update_activity_params
      params.require(:activity).permit(:notes, :planned_for, :done_at)
    end
  end
end
