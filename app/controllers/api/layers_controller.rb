module Api
  class LayersController < ApiApplicationController
    before_action :set_layer, only: [:show, :update, :destroy]

    # GET /layers
    def index
      @layers = policy_scope Layer

      render 'api/layers/index'
    end

    # GET /layers/1
    def show
      render 'api/layers/show'
    end

    # POST /layers
    def create
      authorize Layer
      @layer = Layer.new(layer_params)

      if @layer.save
        render 'api/layers/show', status: :created, location: api_layer_url(@layer)
      else
        render json: @layer.errors, status: :unprocessable_entity
      end
    end

    # PATCH/PUT /layers/1
    def update
      if @layer.update(layer_params)
        render 'api/layers/show', location: api_layer_url(@layer)
      else
        render json: @layer.errors, status: :unprocessable_entity
      end
    end

    # DELETE /layers/1
    def destroy
      @layer.destroy
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_layer
      @layer = Layer.find(params[:id])

      authorize @layer
    end

    # Only allow a trusted parameter "white list" through.
    def layer_params
      params.require(:layer).permit(:name, :description, :color)
    end
  end
end
