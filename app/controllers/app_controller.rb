class AppController < ApplicationController
  before_action :skip_authorization
  before_action :skip_policy_scope
  before_action :authenticate_user!, only: [:app]

  def home; end

  def changelog; end

  def app; end
end
