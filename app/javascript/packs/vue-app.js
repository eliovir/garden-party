import Vue from 'vue'
import App from '../app'
import router from '../vue/routers/app'
import store from '../vue/stores/app'
import i18n from '../vue/helpers/i18n'
import VueBar from 'vuebar'

document.addEventListener('DOMContentLoaded', () => {
  Vue.config.productionTip = false

  Vue.use(VueBar)

  new Vue({
    router,
    i18n,
    store,
    render: h => h(App),
  }).$mount('#app')
})
