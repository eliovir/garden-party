import MarkdownIt from 'markdown-it'
import modifyToken from 'markdown-it-modify-token'

function mdTokenModifier (token) {
  // "Secure" links to the outside world
  if (token.type === 'link_open') {
    const rx = new RegExp(`^https?://${window.location.host}(?:/.*)?$`)

    if (!rx.test(token.attrObj.href)) {
      token.attrObj.target = '_blank'
      token.attrObj.rel = 'noreferrer noopener'
    }
  }
}

const md = new MarkdownIt({
  linkify: true,
  typographer: true,
  modifyToken: mdTokenModifier,
})

md.use(modifyToken)

window.markdown = function (text) {
  return md.render(text)
}

window.renderMD = function (parent, element) {
  const content = element.innerHTML
    .replace(/&gt;/g, '>')
    .replace(/&lt;/g, '<')
  parent.innerHTML = window.markdown(content)
}
