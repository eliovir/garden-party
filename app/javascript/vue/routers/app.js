import Vue from 'vue'
import VueRouter from 'vue-router'
import Maps from '../views/Maps/Index.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'maps',
    component: Maps,
  },
  {
    path: '/library',
    name: 'genera',
    component: () => import(/* webpackChunkName: "genera" */ '../views/Genera/Index.vue'),
  },
  // Maps
  // --------------------------------------------------------------------------
  {
    path: '/maps/new_map',
    name: 'mapForm',
    component: () => import(/* webpackChunkName: "map_form" */ '../views/Maps/Form.vue'),
  },
  {
    path: '/maps/:mapId(\\d+)',
    component: () => import(/* webpackChunkName: "map_show" */ '../views/Layouts/_Garden'),
    children: [
      {
        path: '',
        name: 'map',
        component: () => import(/* webpackChunkName: "map" */ '../views/Maps/Show.vue'),
      },
      {
        path: '/maps/:mapId(\\d+)/patches',
        name: 'patches',
        component: () => import(/* webpackChunkName: "map_overview" */ '../views/Patches/Index.vue'),
      },
      {
        path: '/maps/:mapId(\\d+)/todo',
        name: 'todo',
        component: () => import(/* webpackChunkName: "map_form" */ '../views/Todos/Index.vue'),
      },
      // {
      //   path: '/maps/:mapId(\\d+)/debug',
      //   name: 'debug',
      //   component: () => import(/* webpackChunkName: "debug" */ '../views/Maps/Debug.vue'),
      // },
    ],
  },
]

const router = new VueRouter({
  routes,
})

export default router
