import Vue from 'vue'
import VueToast from 'vue-toast-notification'

Vue.use(VueToast, {
  duration: 5000,
  position: 'bottom-right',
})

export default Vue.$toast
