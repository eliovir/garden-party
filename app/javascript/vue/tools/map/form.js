import { getCenter } from 'ol/extent'
import Projection from 'ol/proj/Projection'
import VectorSource from 'ol/source/Vector'
import VectorLayer from 'ol/layer/Vector'
import StaticSource from 'ol/source/ImageStatic'
import ImageLayer from 'ol/layer/Image'
import Map from 'ol/Map'
import OSM from 'ol/source/OSM'
import TileLayer from 'ol/layer/Tile'
import View from 'ol/View'
import Feature from 'ol/Feature'
import PointGeom from 'ol/geom/Point'

function createVectorLayer (map, center, callBack) {
  const feature = new Feature({ geometry: new PointGeom(center) })

  map.addLayer(new VectorLayer({
    source: new VectorSource({ features: [feature] }),
  }))

  map.on('click', (event) => {
    feature.setGeometry(new PointGeom(event.coordinate))

    callBack(event.coordinate)
  })
}

export default {
  /**
   * Creates a map with a file as background.
   * @param targetId {String}
   * @param extent {Array}
   * @param centerMovedCallback {Function}
   * @returns {Map}
   */
  createMapFromPicture (targetId, file, extent, centerMovedCallback) {
    const imageCenter = getCenter(extent)
    const projection = new Projection({
      code: 'demo-pic',
      units: 'pixels',
      extent: extent,
    })

    const imageSource = new StaticSource({
      url: file,
      projection,
      imageExtent: extent,
    })
    const imageLayer = new ImageLayer({ source: imageSource })

    const theMap = new Map({
      target: targetId,
      layers: [imageLayer],
      view: new View({
        projection,
        center: imageCenter,
        resolution: 1,
        zoom: 2,
        maxZoom: 8,
      }),
    })

    // Give position back, at least once
    centerMovedCallback(imageCenter)

    createVectorLayer(theMap, imageCenter, centerMovedCallback)

    return theMap
  },
  createOSMMap (targetId, centerMovedCallback) {
    const center = [0, 0]

    const theMap = new Map({
      target: targetId,
      layers: [
        new TileLayer({ source: new OSM({}) }),
      ],
      view: new View({
        center,
        zoom: 2,
      }),
    })

    // Give position back, at least once
    centerMovedCallback(center)

    createVectorLayer(theMap, center, centerMovedCallback)

    return theMap
  },
}
