import Toaster from './toaster'
import i18n from '../helpers/i18n'

const ApiErrors = {
  // i18n-tasks-use t('js.api.unauthorized')
  401: 'js.api.unauthorized',
  // i18n-tasks-use t('js.api.forbidden')
  403: 'js.api.forbidden',
  // i18n-tasks-use t('js.api.page_not_found')
  404: 'js.api.page_not_found',
  // i18n-tasks-use t('js.api.unprocessable_entity')
  422: 'js.api.unprocessable_entity',
  // i18n-tasks-use t('js.api.internal_server_error')
  500: 'js.api.internal_server_error',
}

/**
 * @param request {XMLHttpRequest}
 * @param context {Object} Context data for errors translations
 * @return {*}
 */
function handleAPIError (request, context) {
  const status = request.status.toString()
  if (Object.keys(ApiErrors).lastIndexOf(status) > -1) Toaster.error(i18n.t(ApiErrors[status], context))
  // i18n-tasks-use t('js.api.unknown_error')
  else Toaster.error(i18n.t('js.api.unknown_error', { code: status }))

  try {
    return JSON.parse(request.response)
  } catch (SyntaxError) {
    return { error: 'Unprocessable response (not JSON)' }
  }
}

export default handleAPIError
