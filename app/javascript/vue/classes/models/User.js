import callApi from '../../helpers/Api'

/**
 * List of attributes that can be updated in updateAttributes()
 * @type {String[]}
 */
const ATTRIBUTES = ['role', 'email', 'username', 'createdAt', 'updatedAt']

export default class User {
  /* eslint-disable camelcase */
  /**
   * @param id {null|number}
   * @param role {String}
   * @param email {String}
   * @param username {String}
   * @param created_at {Date|String}
   * @param updated_at {Date|String}
   **/
  constructor ({ id = null, role, email, username, created_at, updated_at }) {
    this.id = id
    this.role = role
    this.email = email
    this.username = username
    this.createdAt = created_at ? new Date(created_at) : null
    this.updatedAt = updated_at ? new Date(updated_at) : null
  }
  /* eslint-enable camelcase */

  /**
   * Update from another instance
   * @param newInstance {User}
   */
  updateAttributes (newInstance) {
    ATTRIBUTES.forEach((attribute) => {
      if (newInstance[attribute] !== undefined) this[attribute] = newInstance[attribute]
    })
  }

  /**
   * @return {Promise<User[]>}
   */
  static getIndex () {
    return callApi('get', '/api/users')
      .then(users => users.map(data => new User(data)))
  }

  /**
   * @param id {number}
   * @return {Promise<User>}
   */
  static get (id) {
    return callApi('get', `/api/users/${id}`)
      .then(user => new User(user))
  }

  /**
   * @param payload {Object}
   * @return {Promise<User>}
   */
  static create (payload) {
    return callApi('post', '/api/users', payload)
      .then(user => new User(user))
  }

  /**
   * @param payload {Object}
   * @return {Promise<User>}
   */
  static update (payload) {
    return callApi('put', `/api/users/${payload.id}`, payload)
      .then(user => new User(user))
  }

  /**
   * @param id {number}
   * @return {Promise<null>}
   */
  static destroy (id) { return callApi('delete', `/api/users/${id}`) }
}
