import callApi from '../../helpers/Api'
import Store from '../../stores/app'

/**
 * List of attributes that can be updated in updateAttributes()
 * @type {String[]}
 */
const ATTRIBUTES = ['nature', 'notes', 'subjectId', 'targetId', 'createdAt', 'updatedAt']

export default class ResourceInteraction {
  /* eslint-disable camelcase */
  /**
   * @param id {null|number}
   * @param nature {String}
   * @param notes {String}
   * @param subject_id {number}
   * @param target_id {number}
   * @param created_at {Date|String}
   * @param updated_at {Date|String}
   **/
  constructor ({ id = null, nature, notes, subject_id, target_id, created_at, updated_at }) {
    this.id = id
    this.nature = nature
    this.notes = notes
    this.subjectId = subject_id
    this.targetId = target_id
    this.createdAt = created_at ? new Date(created_at) : null
    this.updatedAt = updated_at ? new Date(updated_at) : null
  }
  /* eslint-enable camelcase */

  /**
   * Update from another instance
   * @param newInstance {ResourceInteraction}
   */
  updateAttributes (newInstance) {
    ATTRIBUTES.forEach((attribute) => {
      if (newInstance[attribute] !== undefined) this[attribute] = newInstance[attribute]
    })
  }

  getSubject () { return Store.getters.resource(this.subjectId) }

  getTarget () { return Store.getters.resource(this.targetId) }

  getOtherResource (resourceId) {
    if (resourceId === this.subjectId) return this.getTarget()
    else if (resourceId === this.targetId) return this.getSubject()

    return null
  }

  /**
   * @return {Promise<ResourceInteraction[]>}
   */
  static getIndex () {
    return callApi('get', '/api/resource_interactions')
      .then(resourceInteractions => resourceInteractions.map(data => new ResourceInteraction(data)))
  }

  /**
   * @param id {number}
   * @return {Promise<ResourceInteraction>}
   */
  static get (id) {
    return callApi('get', `/api/resource_interactions/${id}`)
      .then(resourceInteraction => new ResourceInteraction(resourceInteraction))
  }

  /**
   * @param payload {Object}
   * @return {Promise<ResourceInteraction>}
   */
  static create (payload) {
    return callApi('post', '/api/resource_interactions', payload)
      .then(resourceInteraction => new ResourceInteraction(resourceInteraction))
  }

  /**
   * @param payload {Object}
   * @return {Promise<ResourceInteraction>}
   */
  static update (payload) {
    return callApi('put', `/api/resource_interactions/${payload.id}`, payload)
      .then(resourceInteraction => new ResourceInteraction(resourceInteraction))
  }

  /**
   * @param id {number}
   * @return {Promise<null>}
   */
  static destroy (id) { return callApi('delete', `/api/resource_interactions/${id}`) }
}
