import callApi from '../../helpers/Api'
import Store from '../../stores/app'
import Activity from './Activity'

/**
 * List of attributes that can be updated in updateAttributes()
 * @type {String[]}
 */
const ATTRIBUTES = ['implantedAt', 'resourceId', 'patchId', 'createdAt', 'updatedAt', 'removedAt', 'implantationPlannedFor', 'removalPlannedFor', 'status']

export default class Element {
  /* eslint-disable camelcase */
  /**
   * @param id {null|number}
   * @param implanted_at {Date|String}
   * @param resource_id {number}
   * @param patch_id {number}
   * @param created_at {Date|String}
   * @param updated_at {Date|String}
   * @param removed_at {Date|String}
   * @param implantation_planned_for {Date|String}
   * @param removal_planned_for {Date|String}
   * @param status {String}
   **/
  constructor ({ id = null, implanted_at, resource_id, patch_id, created_at, updated_at, removed_at, implantation_planned_for, removal_planned_for, status }) {
    this.id = id
    this.implantedAt = implanted_at ? new Date(implanted_at) : null
    this.resourceId = resource_id
    this.patchId = patch_id
    this.createdAt = created_at ? new Date(created_at) : null
    this.updatedAt = updated_at ? new Date(updated_at) : null
    this.removedAt = removed_at ? new Date(removed_at) : null
    this.implantationPlannedFor = implantation_planned_for ? new Date(implantation_planned_for) : null
    this.removalPlannedFor = removal_planned_for ? new Date(removal_planned_for) : null
    this.status = status
  }
  /* eslint-enable camelcase */

  /**
   * Update from another instance
   * @param newInstance {Element}
   */
  updateAttributes (newInstance) {
    ATTRIBUTES.forEach((attribute) => {
      if (newInstance[attribute] !== undefined) this[attribute] = newInstance[attribute]
    })
  }

  getResource () { return Store.getters.resource(this.resourceId) }
  getActivities () { return Store.getters.activitiesByElement(this.id) }
  getLastActions (actions) { return Activity.getActions(this.id, actions) }
  getPatch () { return Store.getters.patch(this.patchId) }

  removeFromOLMap () {
    const patch = Store.getters.patch(this.patchId)
    if (patch.geometryType === 'Point') patch.removeFromOLMap()
  }

  isRemoved () { return this.status === 'removed' }
  isImplanted () { return this.status === 'implanted' }
  isPlanned () { return this.status === 'planned' }
  isImplantationPlanned () { return this.isPlanned() && !!this.implantationPlannedFor }
  isRemovalPlanned () { return this.isImplanted() && !!this.removalPlannedFor }

  /**
   * @returns {string}
   */
  getName () { return this.getResource().name }

  /**
   * @param mapId {number}
   * @return {Promise<Object[]>}
   */
  static getIndex (mapId) {
    return callApi('get', `/api/maps/${mapId}/elements`)
      .then(element => element.map(data => new Element(data)))
  }

  /**
   * @param patchId {number}
   * @return {Promise<Element[]>}
   */
  static getPatchIndex (patchId) {
    return callApi('get', `/api/patches/${patchId}/elements`)
      .then(elements => elements.map(data => new Element(data)))
  }

  /**
   * @param id {number}
   * @return {Promise<Element>}
   */
  static get (id) {
    return callApi('get', `/api/elements/${id}`)
      .then(element => new Element(element))
  }

  /**
   * @param payload {Object}
   * @return {Promise<Element>}
   */
  static create (payload) {
    return callApi('post', '/api/elements', payload)
      .then(element => new Element(element))
  }

  /**
   * @param payload {Object}
   * @return {Promise<Element>}
   */
  static update (payload) {
    return callApi('put', `/api/elements/${payload.id}`, payload)
      .then(element => new Element(element))
  }

  /**
   * @param id {number}
   * @return {Promise<null>}
   */
  static destroy (id) { return callApi('delete', `/api/elements/${id}`) }
}
