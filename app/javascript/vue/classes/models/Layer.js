import callApi from '../../helpers/Api'
import Store from '../../stores/app'

/**
 * List of attributes that can be updated in updateAttributes()
 * @type {String[]}
 */
const ATTRIBUTES = ['name', 'description', 'fillColor', 'borderColor', 'createdAt', 'updatedAt']

export default class Layer {
  /* eslint-disable camelcase */
  /**
   * @param id {null|number}
   * @param name {String}
   * @param description {String}
   * @param fill_color {String}
   * @param border_color {String}
   * @param created_at {Date|String}
   * @param updated_at {Date|String}
   **/
  constructor ({ id = null, name, description, fill_color, border_color, created_at, updated_at }) {
    this.id = id
    this.name = name
    this.description = description
    this.borderColor = border_color
    this.fillColor = fill_color
    this.createdAt = created_at ? new Date(created_at) : null
    this.updatedAt = updated_at ? new Date(updated_at) : null
  }
  /* eslint-enable camelcase */

  /**
   * Update from another instance
   * @param newInstance {Layer}
   */
  updateAttributes (newInstance) {
    ATTRIBUTES.forEach((attribute) => {
      if (newInstance[attribute] !== undefined) this[attribute] = newInstance[attribute]
    })
  }

  /**
   * @param layer {VectorLayer}
   */
  setMapLayer (layer) {
    this.mapLayer = layer
    layer.set('layerId', this.id)
  }

  /**
   * @return {Resource[]}
   */
  getResources () { return Store.getters.resourcesByLayer(this.id) }

  /**
   * @return {Resource[]}
   */
  getParentResources () { return Store.getters.parentResourcesByLayer(this.id) }

  /**
   * @return {Promise<Layer[]>}
   */
  static getIndex () {
    return callApi('get', '/api/layers')
      .then(layers => layers.map(data => new Layer(data)))
  }

  /**
   * @param id {number}
   * @return {Promise<Layer>}
   */
  static get (id) {
    return callApi('get', `/api/layers/${id}`)
      .then(layer => new Layer(layer))
  }

  /**
   * @param payload {Object}
   * @return {Promise<Layer>}
   */
  static create (payload) {
    return callApi('post', '/api/layers', payload)
      .then(layer => new Layer(layer))
  }

  /**
   * @param payload {Object}
   * @return {Promise<Layer>}
   */
  static update (payload) {
    return callApi('put', `/api/layers/${payload.id}`, payload)
      .then(layer => new Layer(layer))
  }

  /**
   * @param id {number}
   * @return {Promise<null>}
   */
  static destroy (id) { return callApi('delete', `/api/layers/${id}`) }
}
