import callApi from '../../helpers/Api'
import Store from '../../stores/app'

/**
 * List of attributes that can be updated in updateAttributes()
 * @type {String[]}
 */
const ATTRIBUTES = ['name', 'geometry', 'geometryType', 'mapId', 'createdAt', 'updatedAt']

export default class Patch {
  /* eslint-disable camelcase */
  /**
   * @param id {null|number}
   * @param name {String}
   * @param geometry {Object}
   * @param geometry_type {String}
   * @param map_id {number}
   * @param created_at {Date|String}
   * @param updated_at {Date|String}
   **/
  constructor ({ id = null, name, geometry, geometry_type, map_id, created_at, updated_at }) {
    this.id = id
    this.name = name
    this.geometry = geometry
    this.geometryType = geometry_type
    this.mapId = map_id
    this.createdAt = created_at ? new Date(created_at) : null
    this.updatedAt = updated_at ? new Date(updated_at) : null
  }
  /* eslint-enable camelcase */

  /**
   * Update from another instance
   * @param newInstance {Patch}
   */
  updateAttributes (newInstance) {
    ATTRIBUTES.forEach((attribute) => {
      if (newInstance[attribute] !== undefined) this[attribute] = newInstance[attribute]
    })
  }

  /**
   * Assigns an OpenLayer feature to the patch
   *
   * @param feature {Feature}
   */
  setFeature (feature) {
    feature.setId(this.id)
    this.feature = feature
  }

  removeFromOLMap () {
    if (!this.feature) return

    this.getLayer().mapLayer.getSource().removeFeature(this.feature)
    this.feature = null
  }

  isRemoved () {
    if (this.geometryType !== 'Point') return false

    const element = this.getElements()[0]
    return element.isRemoved()
  }

  /**
   * @return {Element[]}
   */
  getElements () { return Store.getters.elementsByPatchId(this.id) }
  // FIXME: This is clunky; better create a layer for multi element patches
  getLayer () { return this.getElements()[0].getResource().getLayer() }

  /**
   * @return {String}
   */
  getName () {
    if (this.geometryType === 'Point') {
      const name = this.getElements()[0].getResource().name
      if (this.name && name) return `${this.name} (${name})`
      else return name
    }

    return this.name || I18n.t('js.models.patch.generic_name')
  }

  /**
   * @param mapId {number}
   * @return {Promise<Patch[]>}
   */
  static getIndex (mapId) {
    return callApi('get', `/api/maps/${mapId}/patches`)
      .then(patches => patches.map(data => new Patch(data)))
  }

  /**
   * @param id {number}
   * @return {Promise<Patch>}
   */
  static get (id) {
    return callApi('get', `/api/patches/${id}`)
      .then(patch => new Patch(patch))
  }

  /**
   * @param payload {Object}
   * @return {Promise<Patch>}
   */
  static create (payload) {
    return callApi('post', '/api/patches', payload)
      .then(patch => new Patch(patch))
  }

  /**
   * @param payload {Object}
   * @return {Promise<Patch>}
   */
  static update (payload) {
    return callApi('put', `/api/patches/${payload.id}`, payload)
      .then(patch => new Patch(patch))
  }

  /**
   * @param id {number}
   * @return {Promise<null>}
   */
  static destroy (id) { return callApi('delete', `/api/patches/${id}`) }
}
