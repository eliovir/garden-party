import callApi from '../../helpers/Api'

/**
 * List of attributes that can be updated in updateAttributes()
 * @type {String[]}
 */
const ATTRIBUTES = ['name', 'picture', 'center', 'userId', 'createdAt', 'updatedAt']

export default class Map {
  /* eslint-disable camelcase */
  /**
   * @param id {null|number}
   * @param name {String}
   * @param center {{x:number, y:number}}
   * @param user_id {number}
   * @param picture {{url:string, size:number[]}, null}
   * @param created_at {Date|String}
   * @param updated_at {Date|String}
   **/
  constructor ({ id = null, name, picture, center, user_id, created_at, updated_at }) {
    this.id = id
    this.name = name
    this.picture = picture
    this.center = [center.x, center.y]
    this.userId = user_id
    this.createdAt = created_at ? new Date(created_at) : null
    this.updatedAt = updated_at ? new Date(updated_at) : null
  }
  /* eslint-enable camelcase */

  /**
   * Update from another instance
   * @param newInstance {Map}
   */
  updateAttributes (newInstance) {
    ATTRIBUTES.forEach((attribute) => {
      if (newInstance[attribute] !== undefined) this[attribute] = newInstance[attribute]
    })
  }

  isOSM () {
    return this.picture === null
  }

  /**
   * @return {Promise<Map[]>}
   */
  static getIndex () {
    return callApi('get', '/api/maps')
      .then(maps => maps.map(data => new Map(data)))
  }

  /**
   * @param id {number}
   * @return {Promise<Map>}
   */
  static get (id) {
    return callApi('get', `/api/maps/${id}`)
      .then(map => new Map(map))
  }

  /**
   * @param payload {Object}
   * @return {Promise<Map>}
   */
  static create (payload) {
    const formData = new FormData()
    formData.append('map[name]', payload.name)
    formData.append('map[center][]', payload.center[0])
    formData.append('map[center][]', payload.center[1])
    formData.append('map[picture]', payload.picture)

    return callApi('post', '/api/maps', formData)
      .then(map => new Map(map))
  }

  /**
   * @param payload {Object}
   * @return {Promise<Map>}
   */
  static update (payload) {
    return callApi('put', `/api/maps/${payload.id}`, payload)
      .then(map => new Map(map))
  }

  /**
   * @param id {number}
   * @return {Promise<null>}
   */
  static destroy (id) { return callApi('delete', `/api/maps/${id}`) }
}
