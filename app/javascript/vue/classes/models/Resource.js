import callApi from '../../helpers/Api'
import Store from '../../stores/app'

/**
 * List of attributes that can be updated in updateAttributes()
 * @type {String[]}
 */
const ATTRIBUTES = ['name', 'description', 'edible', 'parentId', 'layerId', 'createdAt', 'updatedAt', 'genusId', 'commonNames', 'source', 'pictureName']

export default class Resource {
  /* eslint-disable camelcase */
  /**
   * @param id {null|number}
   * @param name {String}
   * @param description {String}
   * @param edible {Boolean}
   * @param parent_id {number}
   * @param layer_id {number}
   * @param created_at {Date|String}
   * @param updated_at {Date|String}
   * @param genus_id {number}
   * @param common_names {String[]}
   * @param picture_name {String}
   * @param source {String}
   **/
  constructor ({ id = null, name, description, edible, parent_id, layer_id, created_at, updated_at, genus_id, common_names, source, picture_name }) {
    this.id = id
    this.name = name
    this.description = description
    this.edible = edible
    this.parentId = parent_id
    this.layerId = layer_id
    this.pictureName = picture_name
    this.createdAt = created_at ? new Date(created_at) : null
    this.updatedAt = updated_at ? new Date(updated_at) : null
    this.genusId = genus_id
    this.commonNames = common_names
    this.source = source
  }
  /* eslint-enable camelcase */

  /**
   * Update from another instance
   * @param newInstance {Resource}
   */
  updateAttributes (newInstance) {
    ATTRIBUTES.forEach((attribute) => {
      if (newInstance[attribute] !== undefined) this[attribute] = newInstance[attribute]
    })
  }

  getLayer () { return Store.getters.layer(this.layerId) }

  getChildren () { return Store.getters.childResources(this.id) }

  /**
   * @returns {string} Path to resource picture, to use as markers sources
   */
  getPicturePath () {
    return `/pictures/resources/${this.layerId}/${this.pictureName}.svg`
  }

  /**
   * @returns {string} Path to resource smaller picture, to use as zone background pattern
   */
  getPatternPicturePath () {
    return `/pictures/resources/${this.layerId}/${this.pictureName}_pattern.svg`
  }

  /**
   * @return {Promise<Resource[]>}
   */
  static getIndex () {
    return callApi('get', '/api/resources')
      .then(resources => resources.map(data => new Resource(data)))
  }

  /**
   * @param id {number}
   * @return {Promise<Resource>}
   */
  static get (id) {
    return callApi('get', `/api/resources/${id}`)
      .then(resource => new Resource(resource))
  }

  /**
   * @param payload {Object}
   * @return {Promise<Resource>}
   */
  static create (payload) {
    return callApi('post', '/api/resources', payload)
      .then(resource => new Resource(resource))
  }

  /**
   * @param payload {Object}
   * @return {Promise<Resource>}
   */
  static update (payload) {
    return callApi('put', `/api/resources/${payload.id}`, payload)
      .then(resource => new Resource(resource))
  }

  /**
   * @param id {number}
   * @return {Promise<null>}
   */
  static destroy (id) { return callApi('delete', `/api/resources/${id}`) }
}
