import callApi from '../../helpers/Api'
import Store from '../../stores/app'

/**
 * List of attributes that can be updated in updateAttributes()
 * @type {String[]}
 */
const ATTRIBUTES = ['name', 'notes', 'createdAt', 'updatedAt', 'doneAt', 'plannedFor', 'subjectId', 'subjectType']

export default class Activity {
  /* eslint-disable camelcase */
  /**
   * @param id {null|number}
   * @param name {String}
   * @param notes {String}
   * @param created_at {Date|String}
   * @param updated_at {Date|String}
   * @param done_at {Date|String}
   * @param planned_for {Date|String}
   * @param subject_id {number}
   * @param subject_type {String}
   **/
  constructor ({ id = null, name, notes, created_at, updated_at, done_at, planned_for, subject_id, subject_type }) {
    this.id = id
    this.name = name
    this.notes = notes
    this.createdAt = created_at ? new Date(created_at) : null
    this.updatedAt = updated_at ? new Date(updated_at) : null
    this.doneAt = done_at ? new Date(done_at) : null
    this.plannedFor = planned_for ? new Date(planned_for) : null
    this.subjectId = subject_id
    this.subjectType = subject_type
  }
  /* eslint-enable camelcase */

  /**
   * Update from another instance
   * @param newInstance {Activity}
   */
  updateAttributes (newInstance) {
    ATTRIBUTES.forEach((attribute) => {
      if (newInstance[attribute] !== undefined) this[attribute] = newInstance[attribute]
    })
  }

  getSubject () { return Store.getters.activitySubject(this.id) }

  /**
   * @return {Promise<Activity[]>}
   */
  static getIndex (elementId) {
    return callApi('get', `/api/elements/${elementId}/activities`)
      .then(activities => activities.map(data => new Activity(data)))
  }

  static getActions (elementId, actions = []) {
    return callApi('get', `/api/elements/${elementId}/activities`, { last_actions: actions })
      .then(activities => activities.map(data => new Activity(data)))
  }

  static getPending (mapId) {
    return callApi('get', `/api/maps/${mapId}/activities?pending=true`)
      .then(activities => activities.map(data => new Activity(data)))
  }

  /**
   * @param id {number}
   * @return {Promise<Activity>}
   */
  static get (id) {
    return callApi('get', `/api/activities/${id}`)
      .then(activity => new Activity(activity))
  }

  /**
   * @param payload {Object}
   * @return {Promise<Activity>}
   */
  static create (payload) {
    return callApi('post', '/api/activities', payload)
      .then(activity => new Activity(activity))
  }

  /**
   * @param payload {Object}
   * @return {Promise<Activity>}
   */
  static update (payload) {
    return callApi('put', `/api/activities/${payload.id}`, payload)
      .then(activity => new Activity(activity))
  }

  /**
   * @param id {number}
   * @return {Promise<null>}
   */
  static destroy (id) { return callApi('delete', `/api/activities/${id}`) }
}
