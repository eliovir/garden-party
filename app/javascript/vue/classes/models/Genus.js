import callApi from '../../helpers/Api'
import Store from '../../stores/app'

/**
 * List of attributes that can be updated in updateAttributes()
 * @type {String[]}
 */
const ATTRIBUTES = ['name', 'description', 'kingdom', 'createdAt', 'updatedAt', 'source']

export default class Genus {
  /* eslint-disable camelcase */
  /**
   * @param id {null|number}
   * @param name {String}
   * @param description {String}
   * @param kingdom {String}
   * @param created_at {Date|String}
   * @param updated_at {Date|String}
   * @param source {String}
   **/
  constructor ({ id = null, name, description, kingdom, created_at, updated_at, source }) {
    this.id = id
    this.name = name
    this.description = description
    this.kingdom = kingdom
    this.createdAt = created_at ? new Date(created_at) : null
    this.updatedAt = updated_at ? new Date(updated_at) : null
    this.source = source
  }
  /* eslint-enable camelcase */

  /**
   * Update from another instance
   * @param newInstance {Genus}
   */
  updateAttributes (newInstance) {
    ATTRIBUTES.forEach((attribute) => {
      if (newInstance[attribute] !== undefined) this[attribute] = newInstance[attribute]
    })
  }

  getResources () { return Store.getters.resourcesByGenus(this.id) }

  /**
   * @return {Promise<Genus[]>}
   */
  static getIndex () {
    return callApi('get', '/api/genera')
      .then(genera => genera.map(data => new Genus(data)))
  }

  /**
   * @param id {number}
   * @return {Promise<Genus>}
   */
  static get (id) {
    return callApi('get', `/api/genera/${id}`)
      .then(genus => new Genus(genus))
  }

  /**
   * @param payload {Object}
   * @return {Promise<Genus>}
   */
  static create (payload) {
    return callApi('post', '/api/genera', payload)
      .then(genus => new Genus(genus))
  }

  /**
   * @param payload {Object}
   * @return {Promise<Genus>}
   */
  static update (payload) {
    return callApi('put', `/api/genera/${payload.id}`, payload)
      .then(genus => new Genus(genus))
  }

  /**
   * @param id {number}
   * @return {Promise<null>}
   */
  static destroy (id) { return callApi('delete', `/api/genera/${id}`) }
}
