// Base map requirements
import Map from 'ol/Map'
import View from 'ol/View'
import Projection from 'ol/proj/Projection'
import { getCenter } from 'ol/extent'
// For background layer
import ImageLayer from 'ol/layer/Image'
import StaticSource from 'ol/source/ImageStatic'
// For vector layers (plantationLayer representations)
import VectorLayer from 'ol/layer/Vector'
import VectorSource from 'ol/source/Vector'
// For map interactions
import DrawInteraction from 'ol/interaction/Draw'
import Select from 'ol/interaction/Select'
import ModifyInteraction from 'ol/interaction/Modify'
// For feature insertions ("markers", "shapes", ...)
import Feature from 'ol/Feature'
// For drawing shapes
import { DEVICE_PIXEL_RATIO } from 'ol/has'
import CircleGeom from 'ol/geom/Circle'
// For features styling
import Style from 'ol/style/Style'
import FillStyle from 'ol/style/Fill'
import StrokeStyle from 'ol/style/Stroke'
import IconStyle from 'ol/style/Icon'
import TextStyle from 'ol/style/Text'
// For map data import/export
import GeoJSON from 'ol/format/GeoJSON'
// For popups
import Overlay from 'ol/Overlay'
// Only used in debugging methods
import PointGeom from 'ol/geom/Point'
import TileLayer from 'ol/layer/Tile'
import OSMSource from 'ol/source/OSM'

import Store from '../stores/app'

// Static properties are not yet a thing
const PRELOADED_PATTERNS = {}

export default class GardenMap {
  /**
   * Prepares a new map to display.
   *
   * @param mapConfig {Map} Map configuration from API
   * @param overlayElementId {String} DOM element ID of the popup box
   * @param elementId {String} Target DOM element ID
   */
  constructor (mapConfig, overlayElementId, elementId) {
    this.mapConfig = mapConfig
    this.mode = null
    // Interactions
    this.drawInteraction = null
    this.selectInteraction = null
    this.modifyInteraction = null
    // Map element, to bind events
    this.mapElement = document.getElementById(elementId)
    // Overlay component
    this.overlayElementId = overlayElementId
    this.overlay = null

    this._createMap(elementId)
    this._createLayers()
    GardenMap.preloadPatterns()
      .then(() => { this._addPatches() })
  }

  /**
   * Sets map in a "drawing mode", to add points and geometries
   *
   * @param type {String} Shape type. Check code for allowed values
   * @param resource {Resource} Target resource type, to define target layer and style
   */
  draw (type, resource) {
    this.stopModes()
    if (!type || ['Point', 'Circle', 'Polygon'].indexOf(type) === -1) throw new Error(`Unsupported type ${type}`)

    this.mode = 'draw'

    const targetLayer = resource.getLayer().mapLayer
    if (!targetLayer) throw new Error(`Target layer ${resource.layerId} not found`)

    const source = targetLayer.getSource()
    this.drawInteraction = new DrawInteraction({
      source,
      type,
      name: resource.name,
    })

    this.drawInteraction.on('drawend', (event) => {
      if (event.feature.getGeometry().getType() === 'Point') {
        event.feature.setStyle(this._stylePoint(resource.getPicturePath()))
      } else {
        event.feature.setStyle(this._styleFillable(resource.id, resource.getLayer().borderColor))
      }

      this._notify('addpatch', {
        resourceId: resource.id,
        geoJSON: this._geoJSONFromFeature(event.feature),
        feature: event.feature,
      })
    })

    this.gardenMap.addInteraction(this.drawInteraction)
  }

  /**
   * Stops drawing mode
   */
  stopDrawing () {
    if (this.drawInteraction) this.gardenMap.removeInteraction(this.drawInteraction)
    this.drawInteraction = null
  }

  /**
   * Puts map in edition mode
   */
  edit () {
    this.stopModes()

    this.mode = 'edit'

    this.selectInteraction = new Select({ wrapX: false })
    this.selectInteraction.on('select', event => { this.redrawFeatures(event.deselected) })
    this.selectInteraction.on('select', () => { this._notify('select', this._getSelectedResourceIds()) })

    this.modifyInteraction = new ModifyInteraction({ features: this.selectInteraction.getFeatures() })
    this.modifyInteraction.on('modifyend', (event) => {
      for (const feature of event.features.getArray()) {
        this._notify('updatepatch', {
          id: feature.getId(),
          feature,
          geoJSON: this._geoJSONFromFeature(feature),
        })
      }
    })

    this.gardenMap.addInteraction(this.selectInteraction)
    this.gardenMap.addInteraction(this.modifyInteraction)
  }

  /**
   * Stops edition mode
   */
  stopEditing () {
    if (this.selectInteraction) this.gardenMap.removeInteraction(this.selectInteraction)
    if (this.modifyInteraction) this.gardenMap.removeInteraction(this.modifyInteraction)
    this.selectInteraction = null
    this.modifyInteraction = null
    this._notify('select', [])
  }

  /**
   * Puts map in review mode: click somewhere and get info
   */
  review () {
    this.stopModes()

    this.mode = 'review'
    this.gardenMap.addOverlay(this.overlay)
  }

  stopReviewing () {
    if (!this.overlay) return
    this.overlay.setPosition(undefined)
    this.gardenMap.removeOverlay(this.overlay)
  }

  stopModes () {
    this.stopEditing()
    this.stopDrawing()
    this.stopReviewing()
  }

  /**
   * Removes a given feature from its patch
   * @param patch {Patch}
   */
  destroyPatch (patch) {
    patch.getLayer().mapLayer.getSource().removeFeature(patch.feature)
  }

  /**
   * Toggle layers visibility
   * @param ids {number[]} List of enabled layer IDs
   */
  setVisibleLayers (ids) {
    for (const plantationLayer of Store.getters.layers) {
      plantationLayer.mapLayer.setVisible(ids.indexOf(plantationLayer.id) > -1)
    }
  }

  /**
   * Destroy selected features from the map
   * @return {number[]}
   */
  trashSelection () {
    const ids = this._getSelectedResourceIds()

    for (const patch of Store.getters.patchesByIds(ids)) {
      const layer = patch.getLayer().mapLayer
      const source = layer.getSource()
      source.removeFeature(patch.feature)
    }

    // Clear selection state, feature points, etc... by restarting the action.
    this.edit()

    return ids
  }

  /**
   * Preload resources patterns.
   * If not done or done too late, elements backgrounds will be plain red.
   *
   * @param resources {[Resource]} List of Resource to preload
   * @returns {Promise<unknown[]>} Promise to wait before creating resources in maps
   *
   * FIXME: Rework this method
   */
  static preloadPatterns () {
    const resources = Store.getters.resources
    const promises = []
    // Multi resource pattern
    promises.push(new Promise((resolve, reject) => {
      const canvas = document.createElement('canvas')
      const context = canvas.getContext('2d')
      canvas.width = 40 * DEVICE_PIXEL_RATIO
      canvas.height = 40 * DEVICE_PIXEL_RATIO

      const picture = document.createElement('img')
      picture.setAttribute('src', '/pictures/multi_pattern.svg')
      picture.addEventListener('load', () => {
        PRELOADED_PATTERNS.multiPattern = (function () {
          context.fillStyle = context.createPattern(picture, 'no-repeat')
          context.fillRect(0, 0, canvas.width, canvas.height)
          return context.createPattern(canvas, 'repeat')
        })()
        resolve()
      })
      picture.addEventListener('error', (e) => reject(e))
    }))

    for (const resource of resources) {
      promises.push(new Promise((resolve, reject) => {
        const canvas = document.createElement('canvas')
        const context = canvas.getContext('2d')
        canvas.width = 24 * DEVICE_PIXEL_RATIO
        canvas.height = 24 * DEVICE_PIXEL_RATIO

        const picture = document.createElement('img')
        picture.setAttribute('src', resource.getPatternPicturePath())
        picture.addEventListener('load', () => {
          PRELOADED_PATTERNS[resource.id] = (function () {
            context.fillStyle = context.createPattern(picture, 'no-repeat')
            context.fillRect(0, 0, canvas.width, canvas.height)
            return context.createPattern(canvas, 'repeat')
          })()
          resolve()
        })
        picture.addEventListener('error', (e) => reject(e))
      }))
    }

    return Promise.all(promises)
  }

  /**
   * Adds a given Patch to the map
   *
   * @param patch {Patch}
   */
  addPatch (patch) {
    const elements = patch.getElements()
    if (elements.length === 0) return

    const resource = elements[0].getResource()
    const layer = resource.getLayer().mapLayer
    const source = layer.getSource()

    let feature
    if (patch.geometryType === 'Circle') {
      feature = new Feature(new CircleGeom([patch.geometry.geometry.coordinates[0], patch.geometry.geometry.coordinates[1]], patch.geometry.properties.radius))
    } else {
      feature = new GeoJSON().readFeature(patch.geometry)
    }

    feature.setStyle(this._featureStyle(patch))
    feature.setId(patch.id)

    source.addFeature(feature)
    patch.setFeature(feature)
  }

  /**
   * Update a Patch feature
   * @param features {Feature[]}
   */
  redrawFeatures (features) {
    for (const feature of features) {
      const patch = Store.getters.patch(feature.getId())
      feature.setStyle(this._featureStyle(patch))
    }
  }

  // Removes map from element, can be nulled after
  destroy () {
    this.gardenMap.setTarget(null)
  }

  /**
   * For debugging purpose.Draws some elements on the map layers
   */
  debugLayers () {
    const layers = this.gardenMap.getLayers().getArray()
    for (let i = 0; i < layers.length; i++) {
      const plantationLayer = layers[i].get('plantationLayer')
      if (!plantationLayer) continue

      const source = layers[i].getSource()
      const layerNameFeature = new Feature({
        geometry: new PointGeom([0, i * 10]),
      })
      layerNameFeature.setStyle(new Style({
        text: new TextStyle({
          text: plantationLayer.name,
          textColor: plantationLayer.borderColor,
          rotation: Math.PI / 7,
        }),
      }))
      source.addFeature(layerNameFeature)
      source.addFeature(new Feature(new CircleGeom([i * 100, i * 100], 50)))
    }
  }

  /**
   * Initializes the map and insert it in DOM
   *
   * @param elementId {String} Target DOM element ID
   */
  _createMap (elementId) {
    const viewOptions = {
      center: [this.mapConfig.center[0], this.mapConfig.center[1]],
    }

    let extent, projection

    if (this.mapConfig.isOSM()) {
      viewOptions.zoom = 20
    } else {
      extent = [0, 0, this.mapConfig.picture.size[0], this.mapConfig.picture.size[1]]
      projection = new Projection({
        code: 'demo-pic',
        units: 'pixels',
        extent: extent,
      })

      viewOptions.projection = projection
      viewOptions.zoom = 2
    }

    this.gardenMap = new Map({
      target: elementId,
      layers: [],
      view: new View(viewOptions),
    })

    if (this.mapConfig.isOSM()) {
      this.gardenMap.addLayer(new TileLayer({
        source: new OSMSource(),
      }))
    } else {
      this.gardenMap.addLayer(new ImageLayer({
        source: new StaticSource({
          url: this.mapConfig.picture.url,
          projection: projection,
          imageExtent: extent,
        }),
      }))
    }

    // Create the overlay
    this.overlay = new Overlay({
      element: document.getElementById(this.overlayElementId),
      positioning: 'bottom-center',
      stopEvent: false,
      offset: [0, 50],
    })

    // Attach overlay handler
    this.gardenMap.on('singleclick', (event) => {
      if (event.originalEvent.target.tagName !== 'CANVAS') return
      if (this.mode !== 'review') return

      const feature = this.gardenMap.forEachFeatureAtPixel(event.pixel, feature => feature)

      // Do nothing if no feature is selected: the component
      if (feature) {
        const featureExtent = feature.getGeometry().getExtent()
        const coordinates = getCenter(featureExtent)
        this._notify('pointPatch', feature.getId())
        this.overlay.setPosition(coordinates)
      }
    })
  }

  /**
   * @private
   */
  _createLayers () {
    for (const plantationLayer of Store.getters.layers) {
      const source = new VectorSource({ wrapX: false })
      const layer = new VectorLayer({ source })

      // Set default style for new features (only useful for debugging)
      layer.setStyle(new Style({
        fill: new FillStyle({ color: plantationLayer.fillColor }),
        stroke: new StrokeStyle({
          color: plantationLayer.borderColor,
          width: 2,
        }),
      }))

      this.gardenMap.addLayer(layer)
      plantationLayer.setMapLayer(layer)
    }
  }

  /**
   * Adds patches to the map
   */
  _addPatches () {
    for (const patch of Store.getters.activePatches) this.addPatch(patch)
  }

  /**
   * @private
   */
  _getSelectedResourceIds () {
    const resourceIds = []
    this.selectInteraction.getFeatures().getArray().forEach((f) => {
      const id = f.getId()
      if (id) resourceIds.push(id)
    })

    return resourceIds
  }

  _featureStyle (patch) {
    const elements = patch.getElements()
    const resource = elements[0].getResource()
    let patternId, borderColor

    if (patch.getElements().length === 1) {
      patternId = resource.id
      borderColor = resource.getLayer().borderColor
    } else {
      patternId = 'multiPattern'
      borderColor = 'gray'
    }

    if (patch.geometryType === 'Point') {
      return this._stylePoint(resource.getPicturePath())
    } else {
      return this._styleFillable(patternId, borderColor)
    }
  }

  /**
   * @private
   */
  _stylePoint (src) {
    return new Style({
      image: new IconStyle({
        anchor: [0.5, 0.5],
        anchorXUnits: 'fraction',
        anchorYUnits: 'fraction',
        src,
      }),
    })
  }

  /**
   * @private
   */
  _styleFillable (patternId, borderColor) {
    return new Style({
      fill: new FillStyle({ color: GardenMap._getPreloadedPattern(patternId) }),
      stroke: new StrokeStyle({
        color: borderColor,
        width: 2,
        lineDash: [2, 4],
      }),
    })
  }

  /**
   * @private
   */
  _notify (event, data) {
    this.mapElement.dispatchEvent(new CustomEvent(event, { detail: data }))
  }

  /**
   * @private
   */
  _geoJSONFromFeature (feature) {
    let geoJSON
    if (feature.getGeometry().getType() === 'Circle') {
      const geometry = feature.getGeometry().getFlatCoordinates()
      geoJSON = {
        type: 'Feature',
        geometry: {
          type: 'Point',
          coordinates: [geometry[0], geometry[1]],
        },
        properties: { radius: feature.getGeometry().getRadius() },
      }
    } else {
      const formatter = new GeoJSON()
      geoJSON = formatter.writeFeatureObject(feature)
    }

    return geoJSON
  }

  /**
   * @private
   */
  static _getPreloadedPattern (id) {
    return PRELOADED_PATTERNS[id] || 'red'
  }
}
