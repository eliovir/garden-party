import Vue from 'vue'
import Vuex from 'vuex'

import ActivityModule from './modules/ActivityModule'
import ElementModule from './modules/ElementModule'
import GenusModule from './modules/GenusModule'
import LayerModule from './modules/LayerModule'
import MapModule from './modules/MapModule'
import PatchModule from './modules/PatchModule'
import ResourceInteractionModule from './modules/ResourceInteractionModule'
import ResourceModule from './modules/ResourceModule'
import UserModule from './modules/UserModule'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {},
  mutations: {
    resetMapData (state) {
      state.ElementModule.elements = []
      state.PatchModule.patches = []
      state.ActivityModule.activities = []
    },
  },
  actions: {
    resetMapData ({ commit }) { commit('resetMapData') },
  },
  getters: {},
  modules: {
    ActivityModule,
    ElementModule,
    GenusModule,
    LayerModule,
    MapModule,
    PatchModule,
    ResourceInteractionModule,
    ResourceModule,
    UserModule,
  },
})
