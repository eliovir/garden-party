import Patch from '../../classes/models/Patch'

export default {
  state: {
    /** @type {Patch[]} */
    patches: [],
  },
  mutations: {
    setPatch (state, item) {
      const index = state.patches.findIndex(i => i.id === item.id)
      if (index > -1) state.patches[index].updateAttributes(item)
      else state.patches.push(item)
    },
    deletePatch (state, id) {
      const index = state.patches.findIndex(i => i.id === id)
      if (index > -1) state.patches.splice(index, 1)
      else throw new Error(`can't remove patch with id ${id}`)
    },
  },
  actions: {
    loadPatches ({ commit }, mapId) {
      return Patch.getIndex(mapId)
        .then(items => { items.forEach(item => { commit('setPatch', item) }) })
    },
    loadPatch ({ commit }, id) {
      return Patch.get(id)
        .then(item => { commit('setPatch', item) })
    },
    createPatch ({ commit, dispatch, getters }, { payload, feature }) {
      return Patch.create(payload)
        .then(item => {
          return dispatch('loadPatchElements', item.id)
            // Add and return the new patch once its elements are loaded
            .then(() => {
              commit('setPatch', item)
              // Return new instance
              const patch = getters.patch(item.id)
              patch.setFeature(feature)
              return Promise.resolve(patch)
            })
        })
    },
    updatePatch ({ commit, getters }, payload) {
      return Patch.update(payload)
        .then(item => {
          commit('setPatch', item)
          // Return updated instance
          return Promise.resolve(getters.patch(item.id))
        })
    },
    destroyPatch ({ commit }, id) {
      return Patch.destroy(id)
        .then(() => {
          commit('deletePatch', id)
          return Promise.resolve()
        })
    },
  },
  getters: {
    /**
     * @return {Patch[]}
     */
    patches: state => state.patches,
    activePatches: state => state.patches.filter(p => !p.isRemoved()),
    /**
     * @return {function(ids): Patch[]|null}
     */
    patchesByIds: state => ids => state.patches.filter(p => ids.indexOf(p.id) > -1),
    /**
     * @return {function(id): Patch|null}
     */
    patch: state => id => state.patches.find((i) => i.id === id),
  },
}
