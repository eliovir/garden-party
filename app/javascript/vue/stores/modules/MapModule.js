import Map from '../../classes/models/Map'

export default {
  state: {
    /** @type {Map[]} */
    maps: [],
  },
  mutations: {
    setMap (state, item) {
      const index = state.maps.findIndex(i => i.id === item.id)
      if (index > -1) state.maps[index].updateAttributes(item)
      else state.maps.push(item)
    },
    deleteMap (state, id) {
      const index = state.maps.findIndex(i => i.id === id)
      if (index > -1) state.maps.splice(index, 1)
      else throw new Error(`can't remove map with id ${id}`)
    },
  },
  actions: {
    loadMaps ({ commit }) {
      return Map.getIndex()
        .then(items => { items.forEach(item => { commit('setMap', item) }) })
    },
    loadMap ({ commit }, id) {
      return Map.get(id)
        .then(item => { commit('setMap', item) })
    },
    createMap ({ commit, getters }, payload) {
      return Map.create(payload)
        .then(item => {
          commit('setMap', item)
          // Return new instance
          return Promise.resolve(getters.map(item.id))
        })
    },
    updateMap ({ commit, getters }, payload) {
      return Map.update(payload)
        .then(item => {
          commit('setMap', item)
          // Return updated instance
          return Promise.resolve(getters.map(item.id))
        })
    },
    destroyMap ({ commit }, id) {
      return Map.destroy(id)
        .then(() => {
          commit('deleteMap', id)
          return Promise.resolve()
        })
    },
  },
  getters: {
    /**
     * @return {Map[]}
     */
    maps: state => state.maps
      .sort((a, b) => a.name.localeCompare(b.name)),
    /**
     * @return {function(id): Map|null}
     */
    map: state => id => state.maps.find((i) => i.id === id),
  },
}
