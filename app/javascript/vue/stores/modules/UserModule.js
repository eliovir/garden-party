import User from '../../classes/models/User'

export default {
  state: {
    /** @type {User[]} */
    users: [],
  },
  mutations: {
    setUser (state, item) {
      const index = state.users.findIndex(i => i.id === item.id)
      if (index > -1) state.users[index].updateAttributes(item)
      else state.users.push(item)
    },
    deleteUser (state, id) {
      const index = state.users.findIndex(i => i.id === id)
      if (index > -1) state.users.splice(index, 1)
      else throw new Error(`can't remove user with id ${id}`)
    },
  },
  actions: {
    loadUsers ({ commit }) {
      return User.getIndex()
        .then(items => { items.forEach(item => { commit('setUser', item) }) })
    },
    loadUser ({ commit }, id) {
      return User.get(id)
        .then(item => { commit('setUser', item) })
    },
    createUser ({ commit, getters }, payload) {
      return User.create(payload)
        .then(item => {
          commit('setUser', item)
          // Return new instance
          return Promise.resolve(getters.user(item.id))
        })
    },
    updateUser ({ commit, getters }, payload) {
      return User.update(payload)
        .then(item => {
          commit('setUser', item)
          // Return updated instance
          return Promise.resolve(getters.user(item.id))
        })
    },
    destroyUser ({ commit }, id) {
      return User.destroy(id)
        .then(() => {
          commit('deleteUser', id)
          return Promise.resolve()
        })
    },
  },
  getters: {
    /**
     * @return {User[]}
     */
    users: state => state.users
      .sort((a, b) => a.username.localeCompare(b.username)),
    /**
     * @return {function(id): User|null}
     */
    user: state => id => state.users.find((i) => i.id === id),
  },
}
