import Element from '../../classes/models/Element'

export default {
  state: {
    /** @type {Element[]} */
    elements: [],
  },
  mutations: {
    setElement (state, item) {
      const index = state.elements.findIndex(i => i.id === item.id)
      if (index > -1) state.elements[index].updateAttributes(item)
      else state.elements.push(item)
    },
    deleteElement (state, id) {
      const index = state.elements.findIndex(i => i.id === id)
      if (index > -1) state.elements.splice(index, 1)
      else throw new Error(`can't remove element with id ${id}`)
    },
  },
  actions: {
    loadElements ({ commit }, mapId) {
      return Element.getIndex(mapId)
        .then(items => { items.forEach(item => { commit('setElement', item) }) })
    },
    loadPatchElements ({ commit }, patchId) {
      return Element.getPatchIndex(patchId)
        .then(items => { items.forEach(item => { commit('setElement', item) }) })
    },
    loadElement ({ commit }, id) {
      return Element.get(id)
        .then(item => { commit('setElement', item) })
    },
    createElement ({ commit, getters }, payload) {
      return Element.create(payload)
        .then(item => {
          commit('setElement', item)
          // Return new instance
          return Promise.resolve(getters.element(item.id))
        })
    },
    updateElement ({ commit, getters }, payload) {
      return Element.update(payload)
        .then(item => {
          commit('setElement', item)
          // Return updated instance
          return Promise.resolve(getters.element(item.id))
        })
    },
    implantElement ({ dispatch }, { id, date }) {
      const now = new Date()
      const dateObj = new Date(date)
      const payload = { id }
      if (dateObj > now) payload.implantation_planned_for = date
      else payload.implanted_at = date
      return dispatch('updateElement', payload)
    },
    removeElement ({ dispatch }, { id, date }) {
      const now = new Date()
      const dateObj = new Date(date)
      const payload = { id }
      if (dateObj > now) payload.removal_planned_for = date
      else payload.removed_at = date
      return dispatch('updateElement', payload)
    },
    destroyElement ({ commit }, id) {
      return Element.destroy(id)
        .then(() => {
          commit('deleteElement', id)
          return Promise.resolve()
        })
    },
  },
  getters: {
    /**
     * @return {Element[]}
     */
    elements: state => state.elements,
    /**
     * @return {function(patchId): Element[]}
     */
    elementsByPatchId: state => patchId => state.elements.filter(c => c.patchId === patchId),
    /**
     * @return {Element[]}
     */
    nonPlantedElements: state => state.elements.filter(c => c.implantedAt === null),
    /**
     * @return {function(id): Element|null}
     */
    element: state => id => state.elements.find((i) => i.id === id),
  },
}
