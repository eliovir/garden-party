import Resource from '../../classes/models/Resource'

export default {
  state: {
    /** @type {Resource[]} */
    resources: [],
  },
  mutations: {
    setResource (state, item) {
      const index = state.resources.findIndex(i => i.id === item.id)
      if (index > -1) state.resources[index].updateAttributes(item)
      else state.resources.push(item)
    },
    deleteResource (state, id) {
      const index = state.resources.findIndex(i => i.id === id)
      if (index > -1) state.resources.splice(index, 1)
      else throw new Error(`can't remove resource with id ${id}`)
    },
  },
  actions: {
    loadResources ({ commit }) {
      return Resource.getIndex()
        .then(items => { items.forEach(item => { commit('setResource', item) }) })
    },
    loadResource ({ commit }, id) {
      return Resource.get(id)
        .then(item => { commit('setResource', item) })
    },
    createResource ({ commit, getters }, payload) {
      return Resource.create(payload)
        .then(item => {
          commit('setResource', item)
          // Return new instance
          return Promise.resolve(getters.resource(item.id))
        })
    },
    updateResource ({ commit, getters }, payload) {
      return Resource.update(payload)
        .then(item => {
          commit('setResource', item)
          // Return updated instance
          return Promise.resolve(getters.resource(item.id))
        })
    },
    destroyResource ({ commit }, id) {
      return Resource.destroy(id)
        .then(() => {
          commit('deleteResource', id)
          return Promise.resolve()
        })
    },
  },
  getters: {
    /**
     * @return {Resource[]}
     */
    resources: state => state.resources
      .sort((a, b) => a.name.localeCompare(b.name)),
    /**
     * @return {function(layerId): Resource[]}
     */
    parentResourcesByLayer: state => layerId => state.resources.filter(p => !p.parentId && p.layerId === layerId)
      .sort((a, b) => a.name.localeCompare(b.name)),
    /**
     * @return {function(parentId): Resource[]}
     */
    childResources: state => parentId => state.resources.filter(p => p.parentId === parentId)
      .sort((a, b) => a.name.localeCompare(b.name)),
    /**
     * @return {function(layerId): Resource[]}
     */
    resourcesByLayer: state => layerId => state.resources.filter(p => p.layerId === layerId)
      .sort((a, b) => a.name.localeCompare(b.name)),
    /**
     * @return {function(genusId): Resource[]}
     */
    resourcesByGenus: state => genusId => state.resources.filter(p => p.genusId === genusId)
      .sort((a, b) => a.name.localeCompare(b.name)),
    /**
     * @return {function(id): Resource|null}
     */
    resource: state => id => state.resources.find((i) => i.id === id),
  },
}
