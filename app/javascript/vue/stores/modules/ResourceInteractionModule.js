import ResourceInteraction from '../../classes/models/ResourceInteraction'

export default {
  state: {
    /** @type {ResourceInteraction[]} */
    resourceInteractions: [],
  },
  mutations: {
    setResourceInteraction (state, item) {
      const index = state.resourceInteractions.findIndex(i => i.id === item.id)
      if (index > -1) state.resourceInteractions[index].updateAttributes(item)
      else state.resourceInteractions.push(item)
    },
    deleteResourceInteraction (state, id) {
      const index = state.resourceInteractions.findIndex(i => i.id === id)
      if (index > -1) state.resourceInteractions.splice(index, 1)
      else throw new Error(`can't remove resourceInteraction with id ${id}`)
    },
  },
  actions: {
    loadResourceInteractions ({ commit }) {
      return ResourceInteraction.getIndex()
        .then(items => { items.forEach(item => { commit('setResourceInteraction', item) }) })
    },
    loadResourceInteraction ({ commit }, id) {
      return ResourceInteraction.get(id)
        .then(item => { commit('setResourceInteraction', item) })
    },
    createResourceInteraction ({ commit, getters }, payload) {
      return ResourceInteraction.create(payload)
        .then(item => {
          commit('setResourceInteraction', item)
          // Return new instance
          return Promise.resolve(getters.resourceInteraction(item.id))
        })
    },
    updateResourceInteraction ({ commit, getters }, payload) {
      return ResourceInteraction.update(payload)
        .then(item => {
          commit('setResourceInteraction', item)
          // Return updated instance
          return Promise.resolve(getters.resourceInteraction(item.id))
        })
    },
    destroyResourceInteraction ({ commit }, id) {
      return ResourceInteraction.destroy(id)
        .then(() => {
          commit('deleteResourceInteraction', id)
          return Promise.resolve()
        })
    },
  },
  getters: {
    /**
     * @return {ResourceInteraction[]}
     */
    resourceInteractions: state => state.resourceInteractions
      .sort((a, b) => a.name.localeCompare(b.name)), // FIXME: Validate or change sort field

    interactionsByResource: state => resourceId => state.resourceInteractions.filter(i => i.targetId === resourceId || i.subjectId === resourceId),

    likesInteractionsByResource: (state, getters) => resourceId => getters.interactionsByResource(resourceId).filter(i => i.nature === 'mutualism'),

    dislikesInteractionsByResource: (state, getters) => resourceId => getters.interactionsByResource(resourceId).filter(i => i.nature === 'competition'),

    /**
     * @return {function(id): ResourceInteraction|null}
     */
    resourceInteraction: state => id => state.resourceInteractions.find((i) => i.id === id),
  },
}
