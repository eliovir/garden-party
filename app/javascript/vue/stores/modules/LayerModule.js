import Layer from '../../classes/models/Layer'

export default {
  state: {
    /** @type {Layer[]} */
    layers: [],
  },
  mutations: {
    setLayer (state, item) {
      const index = state.layers.findIndex(i => i.id === item.id)
      if (index > -1) state.layers[index].updateAttributes(item)
      else state.layers.push(item)
    },
    deleteLayer (state, id) {
      const index = state.layers.findIndex(i => i.id === id)
      if (index > -1) state.layers.splice(index, 1)
      else throw new Error(`can't remove layer with id ${id}`)
    },
  },
  actions: {
    loadLayers ({ commit }) {
      return Layer.getIndex()
        .then(items => { items.forEach(item => { commit('setLayer', item) }) })
    },
    loadLayer ({ commit }, id) {
      return Layer.get(id)
        .then(item => { commit('setLayer', item) })
    },
    createLayer ({ commit, getters }, payload) {
      return Layer.create(payload)
        .then(item => {
          commit('setLayer', item)
          // Return new instance
          return Promise.resolve(getters.layer(item.id))
        })
    },
    updateLayer ({ commit, getters }, payload) {
      return Layer.update(payload)
        .then(item => {
          commit('setLayer', item)
          // Return updated instance
          return Promise.resolve(getters.layer(item.id))
        })
    },
    destroyLayer ({ commit }, id) {
      return Layer.destroy(id)
        .then(() => {
          commit('deleteLayer', id)
          return Promise.resolve()
        })
    },
  },
  getters: {
    /**
     * @return {Layer[]}
     */
    layers: state => state.layers,
    layersTree: state => (term = '') => {
      term = term.toLowerCase()
      return state.layers.map((l) => {
        l.resources = l.getParentResources().filter((r) => {
          if (r.name.toLowerCase().includes(term)) return true
          return r.commonNames.join('').includes(term)
        })
        return l
      }).filter(l => l.resources.length > 0)
    },
    /**
     * @return {function(id): Layer|null}
     */
    layer: state => id => state.layers.find((i) => i.id === id),
  },
}
