import Activity from '../../classes/models/Activity'

function sortActivities (a, b) {
  const date1 = a.doneAt || a.plannedFor
  const date2 = b.doneAt || b.plannedFor
  return date2 - date1
}

export default {
  state: {
    /** @type {Activity[]} */
    activities: [],
  },
  mutations: {
    setActivity (state, item) {
      const index = state.activities.findIndex(i => i.id === item.id)
      if (index > -1) state.activities[index].updateAttributes(item)
      else state.activities.push(item)
    },
    deleteActivity (state, id) {
      const index = state.activities.findIndex(i => i.id === id)
      if (index > -1) state.activities.splice(index, 1)
      else throw new Error(`can't remove activity with id ${id}`)
    },
  },
  actions: {
    loadActivities ({ commit }, elementId) {
      return Activity.getIndex(elementId)
        .then(items => { items.forEach(item => { commit('setActivity', item) }) })
    },
    loadPendingActivities ({ commit }, mapId) {
      return Activity.getPending(mapId)
        .then(items => { items.forEach(item => { commit('setActivity', item) }) })
    },
    loadActivity ({ commit }, id) {
      return Activity.get(id)
        .then(item => { commit('setActivity', item) })
    },
    createActivity ({ commit, getters }, payload) {
      return Activity.create(payload)
        .then(item => {
          commit('setActivity', item)
          // Return new instance
          return Promise.resolve(getters.activity(item.id))
        })
    },
    updateActivity ({ commit, getters }, payload) {
      return Activity.update(payload)
        .then(item => {
          commit('setActivity', item)
          // Return updated instance
          return Promise.resolve(getters.activity(item.id))
        })
    },
    destroyActivity ({ commit }, id) {
      return Activity.destroy(id)
        .then(() => {
          commit('deleteActivity', id)
          return Promise.resolve()
        })
    },
    finishActivity ({ dispatch }, id) {
      return dispatch('updateActivity', {
        id,
        done_at: new Date().toISOString(),
      })
    },
  },
  getters: {
    /**
     * @return {Activity[]}
     */
    activities: state => state.activities
      .sort((a, b) => sortActivities(a, b)),
    /**
     * @return {function(number): Activity[]}
     */
    activitiesByElement: state => elementId => state.activities.filter(a => a.subjectId === elementId && a.subjectType === 'Element')
      .sort((a, b) => sortActivities(a, b)),
    /**
     * @return {Activity[]}
     */
    pendingActivities: state => state.activities.filter(a => a.doneAt === null)
      .sort((a, b) => sortActivities(a, b))
      .reverse(),
    /**
     * @return {function({elementId?: Number, name: String}): {Activity[]}}
     */
    lastTimeDone: (state, getters) => ({ elementId, name }) => getters.activitiesByElement(elementId).filter(a => a.name === name && !!a.doneAt),
    /**
     * @return {function({elementId?: Number, name: String}): {Activity[]}}
     */
    nextTimePlanned: (state, getters) => ({ elementId, name }) => getters.activitiesByElement(elementId).filter(a => a.name === name && !a.doneAt && !!a.plannedFor),
    /**
     * @return {function(id): Activity|null}
     */
    activity: state => id => state.activities.find((i) => i.id === id),
    /**
     * @return {function(number): (null|Element)}
     */
    activitySubject: (state, getters) => id => {
      const activity = getters.activity(id)
      if (!activity) return null

      switch (activity.subjectType) {
        case 'Element':
          return getters.element(activity.subjectId)
        default:
          return null
      }
    },
  },
}
