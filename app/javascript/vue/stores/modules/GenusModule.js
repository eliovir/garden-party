import Genus from '../../classes/models/Genus'

export default {
  state: {
    /** @type {Genus[]} */
    genera: [],
  },
  mutations: {
    setGenus (state, item) {
      const index = state.genera.findIndex(i => i.id === item.id)
      if (index > -1) state.genera[index].updateAttributes(item)
      else state.genera.push(item)
    },
    deleteGenus (state, id) {
      const index = state.genera.findIndex(i => i.id === id)
      if (index > -1) state.genera.splice(index, 1)
      else throw new Error(`can't remove genus with id ${id}`)
    },
  },
  actions: {
    loadGenera ({ commit }) {
      return Genus.getIndex()
        .then(items => { items.forEach(item => { commit('setGenus', item) }) })
    },
    loadGenus ({ commit }, id) {
      return Genus.get(id)
        .then(item => { commit('setGenus', item) })
    },
    createGenus ({ commit, getters }, payload) {
      return Genus.create(payload)
        .then(item => {
          commit('setGenus', item)
          // Return new instance
          return Promise.resolve(getters.genus(item.id))
        })
    },
    updateGenus ({ commit, getters }, payload) {
      return Genus.update(payload)
        .then(item => {
          commit('setGenus', item)
          // Return updated instance
          return Promise.resolve(getters.genus(item.id))
        })
    },
    destroyGenus ({ commit }, id) {
      return Genus.destroy(id)
        .then(() => {
          commit('deleteGenus', id)
          return Promise.resolve()
        })
    },
  },
  getters: {
    /**
     * @return {Genus[]}
     */
    genera: state => state.genera
      .sort((a, b) => a.name.localeCompare(b.name)),
    /**
     * @return {function(id): Genus|null}
     */
    genus: state => id => state.genera.find((i) => i.id === id),
  },
}
