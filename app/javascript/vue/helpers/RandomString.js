export default function randomString (size = 5) {
  return Math.random().toString(36).substr(2, size)
}
