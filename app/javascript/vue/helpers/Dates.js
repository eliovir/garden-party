/**
 * Checks if a date is the same day as another
 * @param firstDate {Date}
 * @param secondDate {Date}
 * @return {boolean}
 */
export function isSameDay (firstDate, secondDate = new Date()) {
  return (firstDate.getFullYear() === secondDate.getFullYear()) &&
    (firstDate.getMonth() === secondDate.getMonth()) &&
    (firstDate.getDate() === secondDate.getDate())
}

/**
 * Checks if a date is the same month as another
 * @param firstDate {Date}
 * @param secondDate {Date}
 * @return {boolean}
 */
export function isSameMonth (firstDate, secondDate = new Date()) {
  return (firstDate.getFullYear() === secondDate.getFullYear()) &&
    (firstDate.getMonth() === secondDate.getMonth())
}

/**
 * Checks if a date is after another (day-based comparison)
 * @param date {Date}
 * @param reference {Date}
 * @return {boolean}
 */
export function isOver (date, reference = new Date()) {
  // not today and date before reference
  return !isSameDay(reference, date) && date < reference
}
