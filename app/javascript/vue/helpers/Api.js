import i18n from './i18n'
import handleAPIError from '../tools/error_handler'
import Toaster from '../tools/toaster'

/**
 * Makes an Api call and returns a promise with data
 * @param method {String}
 * @param url {String}
 * @param data {Object}
 * @returns {Promise<unknown>}
 */
export default function (method, url, data = {}) {
  if (!data) data = {} // When a null value is passed
  // const token = //
  const request = new window.XMLHttpRequest()

  request.open(method, url, true)
  request.setRequestHeader('Accept', 'application/json')
  if ((typeof data.append) !== 'function') {
    request.setRequestHeader('Content-Type', 'application/json')
    data = JSON.stringify(data)
  }

  // if (token) {
  //   request.setRequestHeader('X-CSRF-TOKEN', token)
  // }
  return new Promise((resolve, reject) => {
    request.onload = function () {
      if (request.status >= 200 && request.status < 400) {
        resolve(request.response ? JSON.parse(request.response) : null)
      } else {
        return reject(handleAPIError(request, { url: url }))
      }
    }
    // FIXME: Handle connectivity issues
    request.onerror = function () {
      // i18n-tasks-use t('js.api.unreachable_server')
      Toaster.error(i18n.t('js.api.unreachable_server'))
      return reject(request)
    }

    request.send(data)
  })
}
