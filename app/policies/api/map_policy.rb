module Api
  class MapPolicy < ApiApplicationPolicy
    def show?
      map_owner?
    end

    def update?
      map_owner?
    end

    def picture?
      map_owner?
    end

    def destroy?
      map_owner?
    end

    class Scope < Scope
      def resolve
        return Map.none if @user.blank?

        scope.where(user: @user).with_attached_picture
      end
    end

    private

    def map_owner?
      return false unless logged_in?

      @record.user == @user
    end
  end
end
