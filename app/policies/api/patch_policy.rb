module Api
  class PatchPolicy < ApiApplicationPolicy
    def show?
      patch_owner?
    end

    def update?
      patch_owner?
    end

    def destroy?
      patch_owner?
    end

    class Scope < Scope
      def resolve
        return Patch.none if @user.blank?

        map_ids = Map.where(user: @user).pluck :id
        scope.where(map: map_ids)
      end
    end

    private

    def patch_owner?
      return false unless logged_in?

      @record.map.user == @user
    end
  end
end
