module Api
  class ResourceInteractionPolicy < ApiApplicationPolicy
    def create?
      admin?
    end

    def update?
      admin?
    end

    def destroy?
      admin?
    end

    class Scope < Scope
      def resolve
        return ResourceInteraction.none if @user.blank?

        scope.all
      end
    end
  end
end
