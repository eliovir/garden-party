module Api
  class ActivityPolicy < ApiApplicationPolicy
    def show?
      map_owner?
    end

    def update?
      map_owner?
    end

    def destroy?
      map_owner?
    end

    class Scope < Scope
      def resolve
        return Activity.none if @user.blank?

        map_ids     = Map.where(user: @user).pluck :id
        patch_ids   = Patch.where(map_id: map_ids).pluck :id
        element_ids = Element.where(patch_id: patch_ids).pluck :id
        scope.where(subject_id: element_ids, subject_type: 'Element')
      end
    end

    private

    def map_owner?
      return false unless logged_in?

      @record.subject.map_owner == @user
    end
  end
end
