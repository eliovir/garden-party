module Admin
  class GenusPolicy < AdminApplicationPolicy
    class Scope < Scope
      def resolve
        scope.all
      end
    end
  end
end
