class FeatureValidator < ActiveModel::EachValidator
  # JSON Schema, from https://github.com/geojson/schema, without the bounding boxes
  # FIXME: Use a lib to validate
  POINT_COORDINATE_FORMAT = {
    type:     'array',
    minItems: 2,
    items:    {
      type: 'number',
    },
  }.freeze
  LINEAR_RING_COORDINATES = {
    type:     'array',
    minItems: 4,
    items:    POINT_COORDINATE_FORMAT,
  }.freeze

  POLYGON_COORDINATE_FORMAT = {
    type:     'array',
    minItems: 1,
    items:    LINEAR_RING_COORDINATES,
  }.freeze

  POINT_FORMAT = {
    title:      'GeoJSON Point',
    type:       'object',
    required:   %w[type coordinates],
    properties: {
      type:        {
        type: 'string',
        enum: ['Point'],
      },
      coordinates: POINT_COORDINATE_FORMAT,
    },
  }.freeze
  POLYGON_FORMAT = {
    title:      'GeoJSON Polygon',
    type:       'object',
    required:   %w[type coordinates],
    properties: {
      type:        {
        type: 'string',
        enum: ['Polygon'],
      },
      coordinates: POLYGON_COORDINATE_FORMAT,
    },
  }.freeze
  FEATURE_FORMAT = {
    title:      'GeoJSON Feature',
    type:       'object',
    required:   %w[type properties geometry],
    properties: {
      type:       {
        type: 'string',
        enum: ['Feature'],
      },
      properties: {
        oneOf: [
          { type: 'null' },
          {
            type:       'object',
            properties: {
              required: ['radius'],
              radius:   {
                type: 'number',
              },
            },
          },
        ],
      },
      geometry:   {
        oneOf: [POINT_FORMAT, POLYGON_FORMAT],
      },
    },
  }.freeze

  def validate_each(record, attribute, value)
    validate_format record, attribute, value

    # Stop validation if format is invalid
    return if record.errors[attribute].size.positive?

    validate_ring(record, attribute, value) if polygon?(value)
    validate_radius(record, attribute, value)
  end

  private

  def polygon?(value)
    value['geometry']['type'] == 'Polygon'
  end

  def validate_format(record, attribute, value)
    schema_errors = JSON::Validator.fully_validate(FEATURE_FORMAT, value)
    record.errors[attribute] << I18n.t('activerecord.errors.feature.bad_feature_format', errors: schema_errors.join("\n")) unless schema_errors.size.zero?
  end

  def validate_radius(record, attribute, value)
    record.errors[attribute] << I18n.t('activerecord.errors.feature.radius_not_allowed') if polygon?(value) && value['properties']&.key?('radius')
  end

  def validate_ring(record, attribute, value)
    polygon_error = value.dig('geometry', 'coordinates', 0).first != value.dig('geometry', 'coordinates', 0).last
    record.errors[attribute] << I18n.t('activerecord.errors.feature.open_polygon') if polygon_error
  end
end
