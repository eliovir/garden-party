class Layer < ApplicationRecord
  validates :name, presence: true
  validates :description, presence: true
  # FIXME: Create custom validator
  validates :color, presence: true

  has_many :resources, dependent: :restrict_with_exception
end
