class Map < ApplicationRecord
  PICTURE_MIMES = ['image/png', 'image/jpeg', 'image/svg+xml'].freeze

  validates :name, presence: true
  validate :center_format
  validate :picture_format

  belongs_to :user
  has_many :patches, dependent: :destroy
  has_one_attached :picture

  before_validation :cleanup_picture

  def picture_size
    return unless picture.attached?

    # ActiveStorages analyzes file and metadata in a job, so we don't have any
    # information unless it's done. We could have analyzed the picture in a
    # after_* hook, but that would mean processing the image twice.
    #
    # This workaround adds a little delay to the first fetch, but none to the next.
    # Note: it won't work _every time_ in a Rails console with Spring started

    # FIXME: In cases where the job fails; we get stuck in a loop
    until picture.analyzed?
      Rails.logger.debug(' > Waiting for picture to be analyzed')
      sleep 1
      reload
    end

    [picture.metadata[:width], picture.metadata[:height]]
  end

  def picture=(value)
    # FIXME: Super effective but super ugly
    super value if value.is_a?(ActionDispatch::Http::UploadedFile) ||
                   value.is_a?(Rack::Test::UploadedFile)
  end

  # Re-define setter as bad values are fed to ActiveRecord::Point otherwise, raising
  # exception before a chance to validate. The drawback is that error message
  # won't be consistent ("empty value", instead of "incorrect value")
  #
  # FIXME: Use a better way to avoid early exceptions
  def center=(value)
    value = nil unless value.is_a?(Array) && value.size == 2
    value&.map!(&:to_f)
    super value
  end

  private

  def cleanup_picture
    # Undocumented Rails API, may change or break in future updates
    return unless attachment_changes['picture']

    path = attachment_changes['picture'].attachable.path

    optimizer = ImageOptim.new(svgo: { enable_plugins: %w[removeScriptElement removeStyleElement removeOffCanvasPaths removeRasterImages] })
    optimizer.optimize_image! path
  end

  def center_format
    errors.add(:center, I18n.t('activerecord.errors.bad_point_format')) unless center.is_a? ActiveRecord::Point
  end

  def picture_format
    return unless picture.attached?

    errors.add(:picture, I18n.t('activerecord.errors.bad_image_format', formats: PICTURE_MIMES.join(', '))) unless PICTURE_MIMES.include? picture.blob.content_type
  end
end
