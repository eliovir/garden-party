class Patch < ApplicationRecord
  validates :geometry, feature: true
  validate :elements_presence

  belongs_to :map
  has_many :elements, dependent: :destroy

  def point?
    return false unless geometry.is_a? Hash

    geometry.dig('geometry', 'type') == 'Point' && !geometry['properties']&.key?('radius')
  end

  def circle?
    return false unless geometry.is_a? Hash

    geometry.dig('geometry', 'type') == 'Point' && geometry['properties']&.key?('radius')
  end

  def polygon?
    return false unless geometry.is_a? Hash

    geometry.dig('geometry', 'type') == 'Polygon'
  end

  def geometry_type
    return 'Polygon' if polygon?
    return 'Circle' if circle?

    'Point'
  end

  private

  def elements_presence
    errors[:elements] << I18n.t('activerecord.errors.models.patch.elements.element_missing') if elements.size.zero?
    errors[:elements] << I18n.t('activerecord.errors.models.patch.elements.too_much_elements') if point? && elements.size > 1
  end
end
