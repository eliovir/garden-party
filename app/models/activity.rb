class Activity < ApplicationRecord
  validates :name, presence: true
  validates :planned_for, presence: true, unless: :done_at
  validates :done_at, presence: true, unless: :planned_for
  validates :subject_type, inclusion: { in: %w[Element] }

  belongs_to :subject, polymorphic: true

  scope :for_map, lambda { |map_id|
    patch_ids   = Patch.where(map_id: map_id).pluck :id
    element_ids = Element.where(patch_id: patch_ids).pluck :id
    where subject_id: element_ids, subject_type: 'Element'
  }
  scope :for_element, ->(element_id) { where(subject_id: element_id, subject_type: 'Element') }
end
