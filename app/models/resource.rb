require 'classes/resource_picture_generator'

class Resource < ApplicationRecord
  enum kingdom: { plant: 0, animal: 1 }

  validates :name, presence: true
  validates :description, presence: true
  validate :common_names_format

  belongs_to :layer
  belongs_to :genus
  belongs_to :parent, class_name: 'Resource', optional: true
  has_many :children, class_name: 'Resource', foreign_key: :parent_id, inverse_of: :parent, dependent: :restrict_with_exception

  after_initialize :set_defaults
  after_destroy :destroy_pictures
  after_save :generate_pictures

  def picture_name
    name.parameterize
  end

  def generate_pictures
    generator = ResourcePictureGenerator.new self
    generator.generate
  end

  def destroy_pictures
    generator = ResourcePictureGenerator.new self
    generator.destroy_pictures
  end

  def common_names=(value)
    value = value.split(',').map(&:strip).uniq if value.is_a? String
    super(value)
  end

  private

  def set_defaults
    self.common_names ||= []
  end

  def common_names_format
    unless common_names.is_a? Array
      errors[:common_names] << I18n.t('activerecord.errors.not_array')
      return
    end

    common_names.each do |name|
      unless name.is_a? String
        errors[:common_names] << I18n.t('activerecord.errors.not_strings_array')
        break
      end
    end
  end
end
