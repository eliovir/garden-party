attributes :id, :name, :description, :created_at, :updated_at

node(:fill_color) { |l| "rgba(#{l.color},0.2)" }
node(:border_color) { |l| "rgb(#{l.color})" }
