attributes :id, :name, :center, :created_at, :updated_at
node(:picture) do |map|
  next nil unless map.picture.attached?

  {
    url:  picture_api_map_url(map),
    size: map.picture_size,
  }
end
